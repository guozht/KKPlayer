#ifndef KKUI_SimpleWnd_H_
#define KKUI_SimpleWnd_H_
#include <assert.h>
#include <Windows.h>
namespace KKUI
{

    class CSimpleWndHelper{
    public:
        static CSimpleWndHelper* GetInstance();

        static BOOL Init(HINSTANCE hInst,LPCTSTR pszClassName);
        static void Destroy();

        HANDLE GetHeap(){return m_hHeap;}

        void LockSharePtr(void * p);
        void UnlockSharePtr();
        void * GetSharePtr(){return m_sharePtr;}

        HINSTANCE GetAppInstance(){return m_hInst;}
        ATOM GetSimpleWndAtom(){return m_atom;}
    private:
        CSimpleWndHelper(HINSTANCE hInst,LPCTSTR pszClassName);
        ~CSimpleWndHelper();

        HANDLE                m_hHeap;
        CRITICAL_SECTION    m_cs;
        void *                m_sharePtr;

        ATOM                m_atom;
        HINSTANCE            m_hInst;

        static CSimpleWndHelper* s_Instance;
    };

#if defined(_M_IX86)
// 按一字节对齐
#pragma pack(push, 1)
struct tagThunk
{
    DWORD m_mov;  // 4个字节
    DWORD m_this;
    BYTE  m_jmp;
    DWORD m_relproc;
    //关键代码   //////////////////////////////////////
    void Init(DWORD_PTR proc, void* pThis)
    {
        m_mov = 0x042444C7;
        m_this = (DWORD)(ULONG_PTR)pThis;  // mov [esp+4], pThis;而esp+4本来是放hWnd,现在被偷着放对象指针了.
        m_jmp = 0xe9;
        // 跳转到proc指定的入口函数
        m_relproc = (DWORD)((INT_PTR)proc - ((INT_PTR)this + sizeof(tagThunk)));
        // 告诉CPU把以上四条语句不当数据，当指令,接下来用GetCodeAddress获得的指针就会运行此指令
        FlushInstructionCache(GetCurrentProcess(), this, sizeof(tagThunk));
    }
    void* GetCodeAddress()
    {
        return this; // 指向this,那么由GetCodeAddress获得的函数pProc是从DWORD m_mov;开始执行的
    }
};
#pragma pack(pop)
#elif defined(_M_AMD64)
#pragma pack(push,2)
struct tagThunk
{
    USHORT  RcxMov;         // mov rcx, pThis
    ULONG64 RcxImm;         //
    USHORT  RaxMov;         // mov rax, target
    ULONG64 RaxImm;         //
    USHORT  RaxJmp;         // jmp target
    void Init(DWORD_PTR proc, void *pThis)
    {
        RcxMov = 0xb948;          // mov rcx, pThis
        RcxImm = (ULONG64)pThis;  //
        RaxMov = 0xb848;          // mov rax, target
        RaxImm = (ULONG64)proc;   //
        RaxJmp = 0xe0ff;          // jmp rax
        FlushInstructionCache(GetCurrentProcess(), this, sizeof(tagThunk));
    }
    //some thunks will dynamically allocate the memory for the code
    void* GetCodeAddress()
    {
        return this;
    }
};
#pragma pack(pop)
#elif defined(_ARM_)
#pragma pack(push,4)
    struct tagThunk // this should come out to 16 bytes
    {
        DWORD    m_mov_r0;        // mov    r0, pThis
        DWORD    m_mov_pc;        // mov    pc, pFunc
        DWORD    m_pThis;
        DWORD    m_pFunc;
        void Init(DWORD_PTR proc, void* pThis)
        {
            m_mov_r0 = 0xE59F0000;
            m_mov_pc = 0xE59FF000;
            m_pThis = (DWORD)pThis;
            m_pFunc = (DWORD)proc;
            // write block from data cache and
            //  flush from instruction cache
            FlushInstructionCache(GetCurrentProcess(), this, sizeof(tagThunk));
        }
        void* GetCodeAddress()
        {
            return this;
        }
    };
#pragma pack(pop)
#else
#error Only AMD64, ARM and X86 supported
#endif

class CMessageMap
{
public:
    virtual BOOL ProcessWindowMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam,
                                      LRESULT& lResult, DWORD dwMsgMapID) = 0;
};

class   CSimpleWnd : public CMessageMap
{
public:
    CSimpleWnd(HWND hWnd=0);
    virtual ~CSimpleWnd(void);

    static ATOM RegisterSimpleWnd(HINSTANCE hInst,LPCTSTR pszSimpleWndName);

    HWND Create(LPCTSTR lpWindowName, DWORD dwStyle,DWORD dwExStyle, int x, int y, int nWidth, int nHeight, HWND hWndParent,LPVOID lpParam );

    BOOL SubclassWindow(HWND hWnd);

    HWND UnsubclassWindow(BOOL bForce /*= FALSE*/);

    const MSG * GetCurrentMessage() const;

    DWORD GetStyle() const;

    DWORD GetExStyle() const;

    LONG_PTR GetWindowLongPtr(int nIndex) const;

    LONG_PTR SetWindowLongPtr(int nIndex, LONG_PTR dwNewLong);

    BOOL ModifyStyle(DWORD dwRemove, DWORD dwAdd, UINT nFlags = 0);

    BOOL ModifyStyleEx(DWORD dwRemove, DWORD dwAdd, UINT nFlags = 0);

    BOOL SetWindowPos(HWND hWndInsertAfter, int x, int y, int cx, int cy, UINT nFlags);

    BOOL CenterWindow(HWND hWndCenter = NULL);

    BOOL DestroyWindow();
    BOOL IsWindow();


    BOOL Invalidate(BOOL bErase = TRUE);
    BOOL InvalidateRect(LPCRECT lpRect, BOOL bErase = TRUE);
    BOOL GetWindowRect(LPRECT lpRect) const;
    BOOL GetClientRect(LPRECT lpRect) const;
    BOOL ClientToScreen(LPPOINT lpPoint) const;

    BOOL ClientToScreen(LPRECT lpRect) const;
    BOOL ScreenToClient(LPPOINT lpPoint) const;

    BOOL ScreenToClient(LPRECT lpRect) const;

    int MapWindowPoints(HWND hWndTo, LPPOINT lpPoint, UINT nCount) const;
    int MapWindowPoints(HWND hWndTo, LPRECT lpRect) const;


    UINT_PTR SetTimer(UINT_PTR nIDEvent, UINT nElapse, void (CALLBACK* lpfnTimer)(HWND, UINT, UINT_PTR, DWORD) = NULL);
    BOOL KillTimer(UINT_PTR nIDEvent);
    HDC GetDC();

    HDC GetWindowDC();

    int ReleaseDC(HDC hDC);

    BOOL CreateCaret(HBITMAP hBitmap);

    BOOL CreateSolidCaret(int nWidth, int nHeight);

    BOOL CreateGrayCaret(int nWidth, int nHeight);

    BOOL HideCaret();
    BOOL ShowCaret();
    HWND SetCapture();

    HWND SetFocus();

    LRESULT SendMessage(UINT message, WPARAM wParam = 0, LPARAM lParam = 0);

    BOOL PostMessage(UINT message, WPARAM wParam = 0, LPARAM lParam = 0);

    BOOL SendNotifyMessage(UINT message, WPARAM wParam = 0, LPARAM lParam = 0);
    BOOL SetWindowText(LPCTSTR lpszString);

    int GetWindowText(LPTSTR lpszStringBuf, int nMaxCount) const;
    BOOL IsIconic() const;

    BOOL IsZoomed() const;

    BOOL IsWindowVisible() const;

    BOOL MoveWindow(int x, int y, int nWidth, int nHeight, BOOL bRepaint = TRUE);

    BOOL MoveWindow(LPCRECT lpRect, BOOL bRepaint = TRUE);

    BOOL ShowWindow(int nCmdShow) ;
    int SetWindowRgn(HRGN hRgn,BOOL bRedraw=TRUE);

    BOOL SetLayeredWindowAttributes(COLORREF crKey,BYTE bAlpha,DWORD dwFlags);
    BOOL UpdateLayeredWindow(HDC hdcDst, POINT *pptDst, SIZE *psize, HDC hdcSrc, POINT *pptSrc,COLORREF crKey, BLENDFUNCTION *pblend,DWORD dwFlags);
    LRESULT DefWindowProc();
    LRESULT ForwardNotifications(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT ReflectNotifications(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    static BOOL DefaultReflectionHandler(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam, LRESULT& lResult);

public://EXTRACT FROM BEGIN_MSG_MAP_EX and END_MSG_MAP
    BOOL ProcessWindowMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam, LRESULT& lResult, DWORD dwMsgMapID = 0);
protected:
    LRESULT DefWindowProc(UINT uMsg, WPARAM wParam, LPARAM lParam);

    virtual void OnFinalMessage(HWND hWnd);

    const MSG * m_pCurrentMsg;
    BOOL m_bDestoryed;
public:

    HWND m_hWnd;
protected:
    static LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMsg,
                                       WPARAM wParam, LPARAM lParam);

    // 只执行一次
    static LRESULT CALLBACK StartWindowProc(HWND hWnd, UINT uMsg,
                                            WPARAM wParam, LPARAM lParam);

    tagThunk *m_pThunk;
    WNDPROC    m_pfnSuperWindowProc;
};

}//namespace SOUI


#endif