#ifndef KKUI_SkinObjBase_H_
#define KKUI_SkinObjBase_H_

#include "UIBase.h"
#include "ObjRef.h"
#include "AttrCracker.h"
#include "ISkinObj.h"
#include "TString.h"
#include "UIApp.h"
namespace KKUI{
	
	class IRenderTarget;

	class CSkinObjBase : public  CSkinObj
    {
		
		KKUI_CLASS_NAME_EX( CSkinObj,CSkinObjBase,"skinobjbase",Skin)
    public:
           CSkinObjBase():m_byAlpha(0xFF),m_bEnableColorize(1),m_crColorize(0), m_nScale(100){}
        /**
        * GetAlpha
        * @brief    获得skin对象包含透明度
        * @return   BYTE -- 透明度
        * Describe  [0-255]
        */    
        BYTE GetAlpha() const
        {
            return m_byAlpha;
        }

        /**
        * SetAlpha
        * @brief    设定skin对象包含透明度
        * @param    BYTE byAlpha-- 透明度
        * @return   void
        * Describe  
        */    
        virtual void SetAlpha(BYTE byAlpha)
        {
            m_byAlpha = byAlpha;
        }

        virtual void Draw(IRenderTarget *pRT, kkRect& rcDraw, unsigned int  dwState,BYTE byAlpha)
        {
            _Draw(pRT,&rcDraw,dwState,byAlpha);
        }

        virtual void Draw(IRenderTarget *pRT, kkRect& rcDraw, unsigned int  dwState)
        {
            Draw(pRT,rcDraw,dwState,GetAlpha());
        }

        virtual kkSize GetSkinSize()
        {
            kkSize ret = {0, 0};

            return ret;
        }

        virtual int IgnoreState()
        {
            return 1;
        }

        virtual int GetStates()
        {
            return 1;
        }

		virtual int       GetScale() const
		{
		   return m_nScale;
		}
		virtual ISkinObj * Scale(int nScale)
		{
			ISkinObj * skinObj = CUIApp::getSingleton().CreateSkinByName(GetObjectClass());
			if(!skinObj) 
				return NULL;
			_Scale(skinObj,nScale);
			return skinObj;
		}

		virtual       SStringA GetName() const {return m_strName;}
        KKUI_ATTRS_BEGIN()
            ATTR_INT("alpha",m_byAlpha,1)   //皮肤透明度
            ATTR_INT("enableColorize",m_bEnableColorize,1)
        KKUI_ATTRS_END()
    protected:

		virtual void _Scale(ISkinObj *pObj, int nScale)
		{
			CSkinObjBase * pSkinObj = UIObj_cast<CSkinObjBase>(pObj);
			pSkinObj->m_nScale = nScale;
			pSkinObj->m_byAlpha = m_byAlpha;
			pSkinObj->m_bEnableColorize = m_bEnableColorize;
			pSkinObj->m_crColorize = m_crColorize;
			pSkinObj->m_strName = m_strName;
		}
        /**
        * _Draw
        * @brief    Draw函数的实现
        * @param    IRenderTarget * pRT --  渲染目标
        * @param    LPCRECT rcDraw --  渲染位置
        * @param    DWORD dwState --  渲染状态
        * @param    BYTE byAlpha --  透明度
        * @return   void
        * Describe  每一个skin需要实现一个_Draw方法
        */    
		virtual void _Draw(IRenderTarget *pRT, kkRect* rcDraw, unsigned int  dwState,BYTE byAlpha) = 0;

        BYTE            m_byAlpha;
        unsigned int    m_crColorize;
        int             m_bEnableColorize;
		int			    m_nScale;
		SStringA	    m_strName;
    };
}

#endif