#ifndef  KKUI_UIYUV_H_
#define  KKUI_UIYUV_H_
#include "UIWindow.h"
#include "RendLock.h"
#include "UIMoveWindow.h"
namespace KKUI
{
	class CUIYUV: public CUIMoveWindow
	{
	    KKUI_CLASS_NAME(CUIMoveWindow,CUIYUV,"uiyuv")
	    public:
			  CUIYUV();
			  ~CUIYUV();
			  int            LoadYUV(kkPicInfo *info);
			   void  OnRButtonUp(unsigned int nFlags,CPoint pt);
			  //CUIWindow*     WndFromPoint( kkPoint  ptHitTest, int bOnlyText);
	    protected:
			   void OnPaint( IRenderTarget  *pRT);
			   void OnPaintEx( IRenderTarget  *pRT,CRect* rt);

			    KKUI_ATTRS_BEGIN()
						ATTR_INT("Alpha",m_nAlpha,0)
						ATTR_INT("ColorR",m_nColorR,0)
						ATTR_INT("ColorG",m_nColorG,0)
						ATTR_INT("ColorB",m_nColorB,0)
				 KKUI_ATTRS_ENDEX(CUIMoveWindow)

			  KKUI_MSG_MAP_BEGIN()   
					MSG_UI_SIZE(OnSize)
					 MSG_UI_RBUTTONUP(OnRButtonUp)
					MSG_UI_PAINT(OnPaint)
					MSG_UI_PAINT_EX(OnPaintEx);
					//MSG_UI_ERASEBKGND(OnEraseBkgnd)
			  KKUI_MSG_MAP_END(CUIMoveWindow)
	    private:
			IPicYuv420p*      m_pIPicYuv420p;
			unsigned char     m_nAlpha;
			unsigned char     m_nColorR;
			unsigned char     m_nColorG;
			unsigned char     m_nColorB;
			
	};
}
#endif