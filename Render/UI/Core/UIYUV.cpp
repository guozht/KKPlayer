#include "UIYUV.h"
namespace KKUI
{
	CUIYUV::CUIYUV():
            m_pIPicYuv420p(0),
			m_nAlpha  (255),
			m_nColorR (255),
			m_nColorG (255),
			m_nColorB (255)
	{
		
	}
	CUIYUV::~CUIYUV()
	{
		//this->Release
	}

	
	void CUIYUV::OnPaint( IRenderTarget  *pRT)
	{
	
		if(m_pIPicYuv420p){
			CRect rt;
			GetClientRect(&rt);
			rt.bottom-=1;
			pRT->DrawYuv420p(&rt,m_pIPicYuv420p,0,0,m_nAlpha,m_nColorR,m_nColorG,m_nColorB);
		}
		
	}
    void CUIYUV::OnPaintEx( IRenderTarget  *pRT,CRect* rt)
	{
	    if(m_pIPicYuv420p){
			pRT->DrawYuv420p(rt,m_pIPicYuv420p,0,0,m_nAlpha,m_nColorR,m_nColorG,m_nColorB);
		}
	}
	
	void CUIYUV::OnRButtonUp(unsigned int nFlags,CPoint pt)
	{
	     UIEvent ev;
		 ev.Handle = this;
		 ev.type   = E_RClick;
		 ev.pa1    = pt.x;
         ev.pa2    = pt.y;
		 GetContainer()->GetIUIMessage()->OnUIEvent(&ev);

	}
	int  CUIYUV::LoadYUV(kkPicInfo *info)
	{
	
		if(m_pIPicYuv420p==0){
			this->GetContainer()->GetIRenderFactory()->CreateYUV420p(&m_pIPicYuv420p);
		}
		m_pIPicYuv420p->Load(info);

	   Invalidate();
	   return 0;
	}
	/*CUIWindow*      CUIYUV::WndFromPoint( kkPoint  ptHitTest, int bOnlyText)
	{
	    return 0;
	}*/
}