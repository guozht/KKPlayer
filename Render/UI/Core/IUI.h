#ifndef IUI_H_
#define IUI_H_
#include "IRender.h"
#include "IUIMessage.h"
//UI控件
typedef void*        KKUIHandle;
//容器控件
typedef void*        KKUIContainer;

//YUV控件
typedef void*        KKUIYUVHandle;

//初始化
typedef void         (*KKUI_Ini)(KKUI::IRenderFactory* rf);

///创建一个UI控件
typedef KKUIHandle   (*KKUI_CreateCtl)         (char* uiname);


//获取控件属性
typedef int          (*KKUI_GetCtlAttr)        (KKUIHandle handle,char* AttrName ,char* OutAttr,int Attrlen);

//设置容器message回调
typedef void         (*KKUI_SetUIMessage)      (KKUIContainer handle,KKUI::IUIMessage* call);

//设置呈现工厂
typedef void         (*KKUI_SetIRenderFactory) (KKUIContainer handle,KKUI::IRenderFactory* pRenderFactory);

//查找控件，根据控件名称
typedef KKUIHandle   (*KKUI_FindCtlName)       (KKUIHandle handle,const char* strValue, int bLoading);


//获取控件名称
typedef int          (*KKUI_GetCtlName)         (KKUIHandle handle,char* OutName, int NameLen);

//获取控件window大小
typedef void         (*KKUI_GetCtlWindowRect)   (KKUIHandle handle,kkRect* OutRect);

//设置控件属性
typedef void         (*KKUI_SetCtlAttr)         (KKUIHandle handle,const char* attr,const char* attrvalue);

//获取用户数据
typedef void*        (*KKUI_GetCtlUserData)     (KKUIHandle handle);
//设置用户数据
typedef void         (*KKUI_SetCtlUserData)     (KKUIHandle handle,void* UserData);

///绘制，绘制的控件必须为容器控件
typedef void         (*KKUI_Opain)              (KKUIContainer handle,KKUI::IRenderTarget* ren);

//舞台控件绘制，必须为舞台控件
typedef void         (*KKUI_StageOpain)         (KKUIHandle handle,KKUI::IRenderTarget* ren);
//请求布局
typedef void         (*KKUI_Relayout)           (KKUIContainer handle,int x,int y,int cx,int cy);
//事件处理
typedef int          (*KKUI_FrameEvent)         (KKUIContainer handle,unsigned int uMsg,unsigned int wParam,long lParam);
//窗口销毁时调用
typedef void         (*KKUI_OnDestroy)          (KKUIContainer handle);

//用xml描述创建控件
typedef KKUIHandle   (*KKUI_CreateChildren)     (KKUIHandle handle,char* strxml);

//删除文件
typedef int          (*KKUI_DeleteCtl)          (KKUIHandle RootHandle,KKUIHandle Handle);

//加载YUV数据
typedef int          (*KKUI_YUVLoad)            (KKUIYUVHandle handle,kkPicInfo *info);



//创建d3d呈现器
typedef void         (*CreateRenderFactoryD3D9) (KKUI::IRenderFactory** ff);

//增加引用计数
typedef int          (*KKUI_AddRef)             (KKUIHandle handle);

//释放引计数
typedef int          (*KKUI_Release)            (KKUIHandle handle);

//获取资源文件大小
typedef int          (*KKUI_GetRawBufferSize)   (char* strType,char* pszResName);

//获取资源
typedef int          (*KKUI_GetRawBuffer)       (char* strType,char* pszResName,void* pBuf,int size);


typedef int          (*KKUI_SetGetResCallInfo)  (KKUI_GetRawBufferSize fp1,KKUI_GetRawBuffer fp2);

//初始化默认皮肤
typedef int          (*KKUI_InitDefSkin)        ();

extern KKUI_Ini                           fpUI_Ini        ;

extern KKUI_CreateCtl                     fpUI_CreateCtl  ;

extern KKUI_GetCtlAttr                    fpUI_GetCtlAttr ;

extern KKUI_SetUIMessage                  fpUI_SetUIMessage;

extern KKUI_SetIRenderFactory             fpUI_SetIRenderFactory;

extern KKUI_FindCtlName                   fpUI_FindCtlName;

extern KKUI_GetCtlName                    fpUI_GetCtlName;

extern KKUI_GetCtlWindowRect              fpUI_GetCtlWindowRect;

extern KKUI_SetCtlAttr                    fpUI_SetCtlAttr;

//获取用户数据
extern KKUI_GetCtlUserData                fpUI_GetCtlUserData;
//设置用户数据
extern KKUI_SetCtlUserData                fpUI_SetCtlUserData;

extern KKUI_Opain                         fpUI_Opain      ;

extern KKUI_StageOpain                    fpUI_StageOpain ;

extern KKUI_Relayout                      fpUI_Relayout   ;

extern KKUI_FrameEvent                    fpUI_FrameEvent ;

extern KKUI_OnDestroy                     fpUI_OnDestroy;

extern KKUI_CreateChildren                fpUI_CreateChildren;

extern KKUI_DeleteCtl                     fpUI_DeleteCtl;

//加载YUV数据
extern KKUI_YUVLoad                       fpUI_YUVLoad;



extern CreateRenderFactoryD3D9            fpCreateRenderFactoryD3D9 ;

extern KKUI_AddRef                        fpUI_AddRef;

extern KKUI_Release                       fpUI_Release;

extern KKUI_SetGetResCallInfo             fpUI_SetGetResCallInfo;
extern KKUI_InitDefSkin                   fpUI_InitDefSkin;
#endif