#include "UIMoveWindow.h"
#include "SplitString.h"
namespace KKUI
{
    CUIMoveWindow::CUIMoveWindow(void):
			m_nFloatHAlign(HALIGN_RIGHT),
			m_nFloatVAlign(VALIGN_BOTTOM),
			m_nDistX(10),
			m_nDistY(10),
			m_bDraging(0)
    {
		m_PtSize =sUnkown;
        m_bFloat =1;
		m_szView.cx = 0;
		m_szView.cy = 0;
    }

    CUIMoveWindow::~CUIMoveWindow(void)
    {
    }

	void CUIMoveWindow::GetWindowRect(kkRect* pRect)  const
	{
	    assert(pRect);
		CRect rcWnd = m_rcWindow;
		*pRect = rcWnd;
	}
	
    void CUIMoveWindow::OnUpdateFloatPosition(const  CSize&  szView,const CRect & rcParent)
    {
		
		
		CRect rt;
		CUIWindow::GetWindowRect(&rt);
		int x=m_szView.cx -szView.cx;
		int y=m_szView.cy -szView.cy;
        m_szView = szView;
		
        CSize sz(GetLayoutParam()->GetSpecifiedSize(Horz).toPixelSize(GetScale()),GetLayoutParam()->GetSpecifiedSize(Vert).toPixelSize(GetScale()));

		
		CRect rcWnd(0,0,sz.cx,sz.cy);
		rcWnd.MoveToX(rt.left+x);
		rcWnd.MoveToY(rt.top+y);

		
		
       // Move(&rcWnd);
		OnRelayout(&rcWnd);
		
    }

    
    void CUIMoveWindow::OnLButtonDown(unsigned int nFlags,CPoint pt)
    {
        SetCapture();
        m_bDraging =1;
		
		CRect rt;
		CUIWindow::GetWindowRect(&rt);
        m_ptClick = pt - rt.TopLeft();

		GetContainer()->GetIUIMessage()->UISetCursor("sizeall");
       // ::SetCursor(GETRESPROVIDER->LoadCursor(_T("sizeall")));
    }

	int CUIMoveWindow::OnSetCursor(const CPoint &pt)
	{
		if(m_bDraging ==1){
		    GetContainer()->GetIUIMessage()->UISetCursor("sizeall");
			return 1;
		}
		return CUIWindow::OnSetCursor(pt);
	}


    void CUIMoveWindow::OnLButtonUp(unsigned int nFlags,CPoint pt)
    {
         ReleaseCapture();
         m_bDraging = 0;

		 UIEvent ev;
		 ev.Handle = this;
		 ev.type   = E_Click;
		 ev.pa1    = 0;

		 GetContainer()->GetIUIMessage()->OnUIEvent(&ev);
    }

	void  CUIMoveWindow::OnNcLButtonUp   (unsigned int nFlags,CPoint pt)
	{
	    ReleaseCapture();
        m_bDraging = 0;


		 UIEvent ev;
		 ev.Handle = this;
		 ev.type   = E_Click;
		 ev.pa1    = 0;

		 GetContainer()->GetIUIMessage()->OnUIEvent(&ev);
	}

    void CUIMoveWindow::OnNcLButtonDown  (unsigned int nFlags,CPoint pt)
	{
	    SetCapture();
        m_bDraging =1;
		
	
        m_ptClick = pt;
		//GetContainer()->GetIUIMessage()->UISetCursor("sizeall");
	}
	
    void CUIMoveWindow::OnMouseMove(unsigned int nFlags,CPoint pt)
    {
        if(m_bDraging)
        {
            CPoint ptLT =pt - m_ptClick;
            CRect rcWnd;
			CUIWindow::GetWindowRect(&rcWnd);
            rcWnd.MoveToXY(ptLT);
            
            //Move(&rcWnd);
			OnRelayout(&rcWnd);
            SStringA Pos;
			Pos.Format("%d,%d,@%d,@%d",rcWnd.left,rcWnd.top,rcWnd.Width(),rcWnd.Height());
			this->SetAttribute("pos", Pos);
			this->GetParent()->UpdateChildrenPosition();

			 UIEvent ev;
			 ev.Handle = this;
			 ev.type   = E_CltMove;
			 ev.pa1    = 0;

		     GetContainer()->GetIUIMessage()->OnUIEvent(&ev);
        }
    }

	
	 void CUIMoveWindow::OnNcMouseMove(unsigned int nFlags,CPoint pt)
	 {
		
		 CRect rcWindow;
		 CUIWindow::GetWindowRect(&rcWindow);

		  if(!m_bDraging){
			  if(pt.x >= rcWindow.right-5 )
			  {
					m_PtSize= sRight;
			  }else if(pt.y  <= rcWindow.top + 5 ){
					 m_PtSize=  sTop;
			  }else if(pt.y  >= rcWindow.bottom - 5 ){
					 m_PtSize=  sBottom;
			  }else {
			       m_PtSize =sLeft;
			  }
		  } 
		  PtSize tt=m_PtSize;
	     if(m_bDraging){


            CPoint ptLT = pt - m_ptClick;
			
			
			if(ptLT.x!=0 || 1)
			{
					m_ptClick = pt;
					CRect rt = rcWindow;
					if(tt==sLeft)
					{
						rt.left += ptLT.x;
					}else if(tt== sRight){
					   rt.right += ptLT.x;
					}else if(tt == sTop)
					{
						rt.top+=ptLT.y;
					}else if(tt == sBottom)
					{
						rt.bottom +=ptLT.y;
					}

					SStringA Pos;
					Pos.Format("%d,%d,@%d,@%d",rt.left,rt.top,rt.Width(),rt.Height());
					this->SetAttribute("pos", Pos);
					OnRelayout(&rt);
					this->GetParent()->UpdateChildrenPosition();

					 UIEvent ev;
					 ev.Handle = this;
					 ev.type   = E_CtlSize;
					 ev.pa1    = 0;

					 GetContainer()->GetIUIMessage()->OnUIEvent(&ev);
			}

		
		}
	   if(tt== sRight|| tt == sLeft)
		   GetContainer()->GetIUIMessage()->UISetCursor("sizewe");
	   else  if(tt== sTop || tt==sBottom)
		  GetContainer()->GetIUIMessage()->UISetCursor("sizens");
	 }

	 void CUIMoveWindow::OnEraseBkgnd( IRenderTarget  *pRT)
	 {
	 	CUIWindow::OnEraseBkgnd( pRT);
	 }
	  int  CUIMoveWindow::OnJustSize(const SStringA& strValue, int bLoading)
	  {

		  SStringAList strLst;
          SplitString(strValue,L',',strLst);
		  if(strLst.size()==4){
			  CRect rcWnd;
			  rcWnd.left   =  atoi(strLst[0]);
			  rcWnd.top    =  atoi(strLst[1]);
			  rcWnd.right  =  rcWnd.left  +  atoi(strLst[2]);
			  rcWnd.bottom =  rcWnd.top  +   atoi(strLst[3]);

			     OnRelayout(&rcWnd);
				SStringA Pos;
				Pos.Format("%d,%d,@%d,@%d",rcWnd.left,rcWnd.top,rcWnd.Width(),rcWnd.Height());
				this->SetAttribute("pos", Pos);
				this->GetParent()->UpdateChildrenPosition();

		  }
	     return 1;
	  }
}
