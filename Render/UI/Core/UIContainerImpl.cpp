#include "UIContainerImpl.h"
#include "UIWindow.h"
namespace KKUI{
	  
	CUIContainerImpl::CUIContainerImpl():m_pIUIMessage(0),
			       m_pCapture(0),
                   m_pHover(0),
				   m_bNcHover(0),
				   m_pRenderFactory(0)
	 {
		 SetContainer(this);
	 }
	 
	 CUIContainerImpl::~CUIContainerImpl()
	 {
		 int i=0;
		 i++;
	 }

	 void      CUIContainerImpl::  ClearHover(CUIWindow  *pChild)
	 {             
		           if(pChild==m_pHover ){
	                   m_pHover =0;
				       m_bNcHover =0;
	               }
				   if(m_pCapture == pChild){
				       OnReleaseUIWndCapture();
				   }
	 }
	 int      CUIContainerImpl::OnReleaseUIWndCapture()
	 {
		 if(m_pCapture)
			 m_pCapture->Release();
		 m_pCapture=NULL;
		 return 1;
	 }
	 void      CUIContainerImpl::SetIRenderFactory(IRenderFactory* pRenderFactory)
	 {
	            m_pRenderFactory = pRenderFactory;
	 }

	 IRenderFactory*   CUIContainerImpl::GetIRenderFactory()
	 {
	 
		 return m_pRenderFactory;
	 }
     CUIWindow*  CUIContainerImpl::OnSetUIWndCapture( CUIWindow* swnd)
	 {
			 if(swnd->IsDisabled(1)) 
				  return 0;
			m_pCapture=swnd;
			if(m_pCapture)
			 m_pCapture->AddRef();
			return swnd;
	 }
	 void CUIContainerImpl::SetMessage(IUIMessage*  mess)
	 {
	   m_pIUIMessage = mess;
	 }

	IUIMessage*  CUIContainerImpl::GetIUIMessage()
	{
	   return m_pIUIMessage;
	}
	int CUIContainerImpl::DoFrameEvent(unsigned int uMsg,unsigned int wParam,long lParam)
	{
	    int lRet=0;
 
		switch(uMsg)
		{
			case UI_MOUSEMOVE:
			    OnFrameMouseMove(wParam,GET_X_LPARAM(lParam),GET_Y_LPARAM(lParam));
			    break;
			case UI_MOUSEHOVER:
				OnFrameMouseEvent(uMsg,wParam,lParam);
				break;
			case UI_MOUSELEAVE:
				OnFrameMouseLeave();
				break;
			case UI_SETCURSOR :
				lRet=OnFrameSetCursor(GET_X_LPARAM(lParam),GET_Y_LPARAM(lParam));
				if(!lRet){
					this->GetIUIMessage()->UISetCursor("arrow");
				}
				break;
		    default:
			   if(uMsg>=UI_MOUSEFIRST && uMsg <= UI_MOUSELAST)
                  OnFrameMouseEvent(uMsg,wParam,lParam);
			   break;
		}
        return lRet;
	}

	void CUIContainerImpl::OnFrameMouseMove(unsigned int uFlag, const int& x,const int& y)
	{
	      //没有设置鼠标捕获
		kkPoint pt={x,y};
		CUIWindow* pCapture=m_pCapture;
		if(pCapture)
		{   
			//有窗口设置了鼠标捕获,不需要判断是否有TrackMouseEvent属性,也不需要判断客户区与非客户区的变化
			CUIWindow * pHover=pCapture->IsContainPoint(pt,0) ?  pCapture : NULL;
			
			if(pHover!=m_pHover){
				//检测鼠标是否在捕获窗口间移动
				CUIWindow *pOldHover=m_pHover;
				m_pHover=pHover;
				if(pOldHover)
				{
					if(m_bNcHover) 
						pOldHover->SendMessage(UI_NCMOUSELEAVE);

					pOldHover->SendMessage(UI_MOUSELEAVE);/**/
				}
				if(pHover && !(pHover->GetState()&WndState_Hover))    
				{
					if(m_bNcHover) 
						pHover->SendMessage(UI_NCMOUSEHOVER,uFlag,MAKELPARAM(pt.x,pt.y));

					pHover->SendMessage(UI_MOUSEHOVER,uFlag,MAKELPARAM(pt.x,pt.y));/**/
				}
			}
			pCapture->SendMessage(m_bNcHover?UI_NCMOUSEMOVE:UI_MOUSEMOVE , uFlag, MAKELPARAM(pt.x,pt.y));
		}else{
				CUIWindow* pHover=WndFromPoint(pt,0);
				if(m_pHover!=pHover)
				{
					//hover窗口发生了变化
					CUIWindow *pOldHover=m_pHover;
					m_pHover=pHover;
					if(pOldHover)
					{
						int bLeave=1;
						if(pOldHover->GetStyle().m_bTrackMouseEvent){
							//对于有监视鼠标事件的窗口做特殊处理
							bLeave = !pOldHover->IsContainPoint(pt,0);
						}

						if(bLeave){
							if(m_bNcHover) 
								pOldHover->SendMessage(UI_NCMOUSELEAVE);
							pOldHover->SendMessage(UI_MOUSELEAVE);
						}
					}

					if(pHover && !pHover->IsDisabled(1) && !(pHover->GetState() & WndState_Hover))
					{
						m_bNcHover=pHover->OnNcHitTest(pt);
						if(m_bNcHover) 
							pHover->SendMessage(UI_NCMOUSEHOVER,uFlag,MAKELPARAM(pt.x,pt.y));
						pHover->SendMessage(UI_MOUSEHOVER,uFlag,MAKELPARAM(pt.x,pt.y));
					}
				}
				else if(pHover && !pHover->IsDisabled(1)){

					//窗口内移动，检测客户区和非客户区的变化
					int bNcHover=pHover->OnNcHitTest(pt);
					if(bNcHover!=m_bNcHover)
					{
						m_bNcHover=bNcHover;
						if(m_bNcHover)
						{
						    pHover->SendMessage(UI_NCMOUSEHOVER,uFlag,MAKELPARAM(pt.x,pt.y));
						}
						else
						{
						    pHover->SendMessage(UI_NCMOUSELEAVE);
						}
					}
				}
			   if(pHover && !pHover->IsDisabled(1))
					pHover->SendMessage(m_bNcHover?UI_NCMOUSEMOVE:UI_MOUSEMOVE , uFlag, MAKELPARAM(pt.x,pt.y));
		}
	}
	void CUIContainerImpl::OnFrameMouseLeave(){
	
	}
	int  CUIContainerImpl::OnFrameSetCursor(const int& x,const int& y)
	{
		CPoint pt;
		pt.x =x;
		pt.y = y;
	     CUIWindow *pCapture= m_pCapture;
		if(pCapture) 
			return pCapture->OnSetCursor(pt);
		else
		{
			CUIWindow *pHover= m_pHover;
			if(pHover && !pHover->IsDisabled(1)) 
				return pHover->OnSetCursor(pt);
		}
		return 0;
	}

	#define UI_NCMOUSEFIRST UI_NCMOUSEMOVE
    #define UI_NCMOUSELAST  UI_NCMBUTTONDBLCLK
	void CUIContainerImpl::OnFrameMouseEvent(unsigned int  uMsg,unsigned int wParam,long lParam)
	{
		
		CUIWindow *pCapture=m_pCapture;
		if(pCapture)
		{
			if(m_bNcHover) 
				uMsg += (unsigned int)UI_NCMOUSEFIRST - UI_MOUSEFIRST;//转换成NC对应的消息
			int bMsgHandled = 0;
			pCapture->SendMessage(uMsg,wParam,lParam,&bMsgHandled);
			SetMsgHandled(bMsgHandled);
		}
		else
		{
			m_pHover=WndFromPoint(CPoint(GET_X_LPARAM(lParam),GET_Y_LPARAM(lParam)),0);
			CUIWindow *pHover=m_pHover;
			if(pHover  && !pHover->IsDisabled(1))
			{
				int bMsgHandled = 0;
				if(m_bNcHover) 
					uMsg += (unsigned int)UI_NCMOUSEFIRST - UI_MOUSEFIRST;//转换成NC对应的消息
				pHover->SendMessage(uMsg,wParam,lParam,&bMsgHandled);
				SetMsgHandled(bMsgHandled);
			}else
			{
				SetMsgHandled(0);
			}
		}
	}
	void CUIContainerImpl::OnFrameMouseWheel(unsigned int  uMsg,unsigned int wParam,long lParam)
	{
	
	}
	
}