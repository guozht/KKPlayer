#ifndef UIMoveState_H_
#define UIMoveState_H_
#include "UIMoveWindow.h"
#include <vector>
//舞台控件
namespace KKUI
{
    class  CUIMoveState: public  CUIMoveWindow
    {
        KKUI_CLASS_NAME( CUIMoveWindow,CUIMoveState,"uimovestate")
		public:
				 CUIMoveState(void);
				 ~CUIMoveState(void);
				 //舞台绘制
	             void  OnStatgePaint(IRenderTarget  *pRT);
				 //清理相交的控件
				 void  ClearIntersectCtl();
				 //添加相交的控件
				 void  AddIntersectCtl(CUIWindow* ctl);

				 void           DelIntersectCtl(CUIWindow* ctl);
				 void           OnUpdateFloatPosition(const  CSize&  szView,const CRect & rcParent);
				 CUIWindow*     WndFromPoint( kkPoint  ptHitTest, int bOnlyText);
		protected:
				 void  OnPaint( IRenderTarget  *pRT);
				 void  OnEraseBkgnd( IRenderTarget  *pRT);
				 void  OnSize(unsigned int nType, int x,int y);
				 void  OnRButtonUp(unsigned int nFlags,CPoint pt);
				 int   OnStateName(const SStringA& strValue, int bLoading);
				 KKUI_ATTRS_BEGIN()
						ATTR_CUSTOM("StateName",OnStateName)
						ATTR_INT("DisplayScale",m_nDisplayScale,0)
				 KKUI_ATTRS_ENDEX(CUIMoveWindow)

				 KKUI_MSG_MAP_BEGIN()
						 MSG_UI_PAINT(OnPaint)
						 MSG_UI_SIZE(OnSize)
						 MSG_UI_RBUTTONUP(OnRButtonUp)
						 MSG_UI_ERASEBKGND(OnEraseBkgnd)
				 KKUI_MSG_MAP_END(CUIMoveWindow)

				 void LoadData( IRenderTarget  *pRT);
		 private: 
			     std::vector<CUIWindow*>  m_IntersectCtlVec;
				 IBitmap*    m_pBitmapLefTop;
				 IBitmap*    m_pBitmapRightBu;
				 IBitmap*    m_pBitmapCenter;
				 IFont*      m_pFont;
				 int         m_nNeedData;
				 int         m_nDisplayScale;
				 SStringA    m_strStateName;
				
	
    };

}

#endif