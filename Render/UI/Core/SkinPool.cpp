#include "SkinPool.h"
#include "UIApp.h"
namespace KKUI{
	CSkinKey::CSkinKey():	strName(""),scale(0)
	{
	
	}

	CSkinPool::CSkinPool()
	{
	
	}
    CSkinPool::~CSkinPool()
	{
	
	}
	ISkinObj* CSkinPool::GetSkin(SStringA strSkinName,int nScale)
	{
		char* da= strSkinName.GetBuffer(64);
	    CSkinKey key;
		key.strName= da;
		key.scale=nScale;
		if(!HasKey(key))
		{
				return 0;
		}

		return GetKeyObject(key);
	}
	
	int  CSkinPool::LoadSkins(pugi::xml_node xmlNode)
	{
	       if(!xmlNode) 
			   return 0;
    
			int nLoaded=0;
			SStringA strSkinName, strTypeName;

			//loadSkins前把this加入到poolmgr,便于在skin中引用其它skin
			CSkinPoolMgr::getSingleton().PushSkinPool(this);

			pugi::xml_node xmlSkin=xmlNode.first_child();
			while(xmlSkin)
			{
				strTypeName = xmlSkin.name();
				strSkinName = xmlSkin.attribute("name").value();

				if (strSkinName.IsEmpty() || strTypeName.IsEmpty())
				{
					xmlSkin=xmlSkin.next_sibling();
					continue;
				}
		            
		        
				ISkinObj *pSkin=CUIApp::getSingleton().CreateSkinByName(strTypeName.GetBuffer(64));
				if(pSkin)
				{
					pSkin->InitFromXml(xmlSkin);
					CSkinKey key;
					key.strName= strSkinName.GetBuffer(64);
					key.scale= pSkin->GetScale();

					assert(!HasKey(key));
					AddKeyObject(key,pSkin);
					nLoaded++;
				}
				else
				{
					//SASSERT_FMTW(FALSE,L"load skin error,type=%s,name=%s",strTypeName,strSkinName);
				}
				xmlSkin=xmlSkin.next_sibling();
             }

			//由于push时直接把this加入tail，为了防止重复调用，这里传NULL,直接从tail删除。
			CSkinPoolMgr::getSingleton().PopSkinPool(NULL);
			return nLoaded;
	}
    void CSkinPool::OnKeyRemoved(const PSkinPtr & obj)
	{
	
	}



	template<> CSkinPoolMgr * CSingleton<CSkinPoolMgr>::m_Singleton=0;

	CSkinPoolMgr::CSkinPoolMgr()
	{
	  
		m_bulitinSkinPool.Attach(new CSkinPool);
        PushSkinPool(m_bulitinSkinPool);

	}
    CSkinPoolMgr::~CSkinPoolMgr()
	{
	

		std::list<CSkinPool *> ::iterator It =  m_lstSkinPools.begin();
		while(It!= m_lstSkinPools.end())
		{
			CSkinPool *p = *It;
			p->Release();
		}
		m_lstSkinPools.clear();
	}

	ISkinObj*  CSkinPoolMgr::GetSkin(const  SStringA& strSkinName,int nScale)
	{
	    std::list<CSkinPool *> ::iterator It =  m_lstSkinPools.begin();
		while(It!= m_lstSkinPools.end())
		{
			CSkinPool *p = *It;
			if(ISkinObj* pSkin=p->GetSkin(strSkinName,nScale))
			{
				return pSkin;
			}
			It++;
		}

		if(strcmp(strSkinName,"")!=0)
		{
			//SASSERT_FMTW(FALSE,L"GetSkin[%s] Failed!",strSkinName);
		}
		return NULL;
	}

	
    void       CSkinPoolMgr::PushSkinPool(CSkinPool *pSkinPool)
	{
		m_lstSkinPools.push_back(pSkinPool);
		pSkinPool->AddRef();
	}
    CSkinPool * CSkinPoolMgr::PopSkinPool(CSkinPool *pSkinPool)
	{
	    CSkinPool * pRet=NULL;
		if(pSkinPool)
		{
			std::list<CSkinPool *>::iterator it = std::find(m_lstSkinPools.begin(), m_lstSkinPools.end(), pSkinPool);
			if( it !=m_lstSkinPools.end()){
				pRet=*it;
				m_lstSkinPools.erase(it);
			}
		}else
		{
			pRet = m_lstSkinPools.back();
			m_lstSkinPools.pop_back();
		}
		if(pRet) 
			pRet->Release();

		return pRet;
	}
}