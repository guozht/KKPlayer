﻿#include "ILayout.h"
#include "LayoutSize.h"
#include <math.h>               // this is where I would normally get the warning message


namespace KKUI
{
	static const char* s_pszUnit[] =
	{
		"px","dp","dip","sp"
	};

	CLayoutSize::CLayoutSize() :fSize(0.0f),unit(px)
	{

	}

	CLayoutSize::CLayoutSize(float _fSize,Unit _unit):fSize(_fSize),unit(_unit)
	{
	}

	static int fround(float v)
	{
		return (int)floor(v+0.5f);
	}

	bool CLayoutSize::fequal(float a, float b)
	{
		return fabs(a-b)<0.00000001f;
	}

	bool CLayoutSize::valueEqual(float value)
	{
		return fequal(fSize,value);
	}

	SStringA CLayoutSize::toString() const
	{
		SStringA strValue = SStringA().Format("%f",fSize);
		//去掉sprintf("%f")生成的小数点最后无效的0
		char* pszData = strValue.GetBuffer(100);
		for(int i=strValue.GetLength()-1;i>=0;i--)
		{
			if(pszData[i]!='0')
			{
				if(pszData[i]=='.') i--;
				strValue = strValue.Left(i+1);
				break;
			}
		}
		return SStringA().Format("%s%s",strValue,s_pszUnit[unit]);
	}


	bool CLayoutSize::isMatchParent() const
	{
		return fequal(fSize , SIZE_MATCH_PARENT);
	}

	void CLayoutSize::setMatchParent()
	{
		fSize = SIZE_MATCH_PARENT;
	}

	bool CLayoutSize::isWrapContent() const
	{
		return fequal(fSize , SIZE_WRAP_CONTENT);
	}

	void CLayoutSize::setWrapContent()
	{
		fSize = SIZE_WRAP_CONTENT;
	}

	void CLayoutSize::setSize(float fSize, Unit unit)
	{
		this->fSize = fSize;
		this->unit = unit;
	}

	bool CLayoutSize::isSpecifiedSize() const
	{
		return fSize>=SIZE_SPEC;
	}

	int CLayoutSize::toPixelSize(int scale) const
	{
		if(isMatchParent()) 
			return SIZE_MATCH_PARENT;
		else if(isWrapContent()) 
			return SIZE_WRAP_CONTENT;
		else if (unit == px)
			return (int)fSize;
		else//if(unit == dp || unit == dip || unit= sp)
			return (int)fround(fSize*scale / 100);
	}

	void CLayoutSize::setInvalid()
	{
		fSize = SIZE_UNDEF;
	}

	bool CLayoutSize::isValid() const
	{
		return !fequal(fSize,SIZE_UNDEF);
	}

	bool CLayoutSize::isZero() const
	{
		return fequal(fSize, 0.0f);
	}

	void CLayoutSize::parseString(const SStringA & strSize)
	{
		if(strSize.IsEmpty()) 
			return;
		SStringA strUnit = strSize.Right(2);
		strUnit.MakeLower();
		unit = px;
		int count=4;
		for(int i=0; i< count;i++)
		{
			const char *xx=s_pszUnit[i];
			if(strUnit.Compare(xx) == 0)
			{
				unit = (Unit)i;
				break;
			}
		}
		fSize = (float)atof(strSize);
	}

	//只复制数值,不复制方向
	CLayoutSize & CLayoutSize::operator=(const CLayoutSize & src)
	{
		fSize = src.fSize;
		unit = src.unit;
		return *this;
	}

	CLayoutSize CLayoutSize::fromString(const SStringA & strSize)
	{
		CLayoutSize ret;
		ret.parseString(strSize);
		return ret;
	}

}
