﻿#ifndef KKUI_ILayout_H_
#define KKUI_ILayout_H_
#include "Core/IObject.h"
#include "Core/IObjRef.h"
#include "Core/ObjectImp.h"
#include "ObjRef.h"
namespace KKUI{
    class CUIWindow;
	class CLayoutSize;
    class CSize;

    enum ORIENTATION{
        Horz,Vert,Any,Both,
    };
    
	enum{
		SIZE_UNDEF = -3,
		SIZE_WRAP_CONTENT=-1,
		SIZE_MATCH_PARENT=-2,
		SIZE_SPEC = 0,
	};

    class ILayoutParam :public IObjRef, public IObject
    {
		public:
			virtual void Clear() = 0;
			virtual bool IsMatchParent(ORIENTATION orientation) const = 0;
			virtual bool IsWrapContent(ORIENTATION orientation) const = 0;
			virtual bool IsSpecifiedSize(ORIENTATION orientation) const = 0;
			virtual CLayoutSize GetSpecifiedSize(ORIENTATION orientation) const = 0;
			virtual void SetMatchParent(ORIENTATION orientation) = 0;
			virtual void SetWrapContent(ORIENTATION orientation) = 0;
			virtual void SetSpecifiedSize(ORIENTATION orientation, const CLayoutSize& layoutSize) = 0;
			virtual void * GetRawData() = 0;
    };

    class ILayout :public IObject ,public IObjRef
	{
		public:
			virtual ILayoutParam * CreateLayoutParam() const = 0;
			virtual bool   IsParamAcceptable(ILayoutParam *pLayoutParam) const=0;
			virtual void LayoutChildren(CUIWindow * pParent) = 0 ;
			virtual CSize MeasureChildren(CUIWindow* pParent,int nWidth,int nHeight) const=0;
    };
	typedef  TObjectImpl<TObjRefImpl<ILayoutParam> > CLayoutParam;
	typedef  TObjectImpl<TObjRefImpl<ILayout> >      CLayout;
}
#endif
