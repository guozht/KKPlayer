#include "UIMoveState.h"
void charTowchar2(const char *chr, wchar_t *wchar, int size) ;
namespace KKUI
{
	CUIMoveState::CUIMoveState(void):
       m_pBitmapLefTop(0),
	   m_pBitmapRightBu(0), 
	   m_pBitmapCenter(0),
	   m_nNeedData(0),
	   m_pFont(0),
	   m_nDisplayScale(4)
	{
	
	}
	CUIMoveState::~CUIMoveState(void)
	{
	           ClearIntersectCtl();

			   if(m_pBitmapLefTop)
				 m_pBitmapLefTop->Release();
			   m_pBitmapLefTop = 0;
			   
			   if(m_pBitmapRightBu)
				   m_pBitmapRightBu->Release();
			   m_pBitmapRightBu = 0;

			   if(m_pBitmapCenter)
				   m_pBitmapCenter->Release();
			    m_pBitmapCenter = 0;

	}

	void  CUIMoveState::ClearIntersectCtl()
	{
		std::vector<CUIWindow*>::iterator It= m_IntersectCtlVec.begin();
		for(;It!=m_IntersectCtlVec.end();++It)
		{
		     CUIWindow* Item=*It;
			 Item->Release();
		}
		m_IntersectCtlVec.clear();
	}
	void   CUIMoveState::AddIntersectCtl(CUIWindow* ctl)
	{
		ctl->AddRef();
		m_IntersectCtlVec.push_back(ctl);
	}
	void   CUIMoveState::DelIntersectCtl(CUIWindow* ctl)
	{
	    std::vector<CUIWindow*>::iterator It= m_IntersectCtlVec.begin();
		for(;It!=m_IntersectCtlVec.end();++It)
		{
		     CUIWindow* Item=*It;
			 if(ctl ==Item){
			    m_IntersectCtlVec.erase(It);
				break;
			 }
		}
	}
    void  CUIMoveState::OnUpdateFloatPosition(const  CSize&  szView,const CRect & rcParent)
	{
		CUIMoveWindow::OnUpdateFloatPosition(szView,rcParent);
	}
    CUIWindow*   CUIMoveState::WndFromPoint( kkPoint  ptHitTest, int bOnlyText)
	{
	    if(!IsContainPoint(ptHitTest,0))
			return NULL;

		if(!IsContainPoint(ptHitTest,1))
			return this;//只在鼠标位于客户区时，才继续搜索子窗口

		std::vector<CUIWindow*>::iterator It= m_IntersectCtlVec.begin();
		for(;It!=m_IntersectCtlVec.end();++It)
		{
		     CUIWindow* pChild=*It;
			if (pChild->IsVisible() && !pChild->IsMsgTransparent())
			{
				CUIWindow  *swndChild = pChild->WndFromPoint(ptHitTest, bOnlyText);

				if (swndChild) 
					return swndChild;
			}

		}

		
		return this;
	}
	void  CUIMoveState::OnSize(unsigned int nType, int x,int y)
	{
		    m_nNeedData=1;
	}
	void  CUIMoveState::OnRButtonUp(unsigned int nFlags,CPoint pt)
	{
	     UIEvent ev;
		 ev.Handle = this;
		 ev.type   = E_RClick;
		 ev.pa1    = pt.x;
         ev.pa2    = pt.y;
		 GetContainer()->GetIUIMessage()->OnUIEvent(&ev);

	}
	int   CUIMoveState::OnStateName(const SStringA& strValue, int bLoading)
	{
		    m_strStateName= strValue;
	    	m_nNeedData=1;
			return 1;
	}
	void  CUIMoveState::LoadData( IRenderTarget  *pRT)
	{
	    kkRect rt;
		this->GetWindowRect(&rt);
		

        //left
	    wchar_t temp[128] = L"";
		swprintf_s( temp,128,L"(%d,%d)",rt.left,rt.top);
		
		kkSize sz;
		pRT->MeasureText(m_pFont,temp,&sz);
		rt.top  += 1;
		rt.left += 1;
		rt.right=rt.left+sz.cx;
		rt.bottom = rt.top+sz.cy;
		pRT->DrawText(m_pFont,temp,&rt);
      
		

		//right
		memset(temp,0,128);
		swprintf_s(temp,128,L"(%d,%d)", rt.right, rt.bottom);	
		pRT->MeasureText(m_pFont,temp,&sz);
        this->GetWindowRect(&rt);
		rt.right-=5;
		rt.left=rt.right-sz.cx;
		rt.bottom -=2;
		rt.top = rt.bottom - sz.cy;
        pRT->DrawText(m_pFont,temp,&rt);

		
		//center;
		memset(temp, 0, 128);
		char* lx=(char*)m_strStateName.GetBuffer(64);
		charTowchar2(lx, temp, 128);
        
		pRT->MeasureText(m_pFont,temp,&sz);
        this->GetWindowRect(&rt);
		rt.left = rt.left+(rt.right-rt.left)/2-sz.cx/2;
		rt.top = rt.top+(rt.bottom-rt.top)/2 -sz.cy/2;
		rt.right=rt.left+sz.cx;
		rt.bottom = rt.top+sz.cy;
        pRT->DrawText(m_pFont,temp,&rt);
		
	}
	void  CUIMoveState::OnStatgePaint( IRenderTarget  *pRT)
	{
		CRect StageRt;
		GetWindowRect(&StageRt);
		int left = StageRt.left;
		int top  = StageRt.top;
		StageRt.MoveToXY(0,0);
	    std::vector<CUIWindow*>::iterator It= m_IntersectCtlVec.begin();
		for(;It!=m_IntersectCtlVec.end();++It)
		{
		     CUIWindow* Item=*It;
		     CRect rt;
			 Item->GetWindowRect(&rt);
			 int ox= rt.left - left;
			 int oy= rt.top - top;
			 rt.MoveToXY(ox,oy);
			 rt.left*=m_nDisplayScale;
			 rt.top*=m_nDisplayScale;
			 rt.right*=m_nDisplayScale;
			 rt.bottom*=m_nDisplayScale;
             Item->SendMessage(UI_PAINT_EX, (unsigned int)pRT, (unsigned int)&rt);
		}
		//m_IntersectCtlVec.clear();
	}
	void  CUIMoveState::OnPaint( IRenderTarget  *pRT)
	{
		if(m_pFont==0){
			this->GetContainer()->GetIRenderFactory()->CreateFont(&m_pFont,12,L"宋体");
		}
		LoadData( pRT);
	}
	void  CUIMoveState::OnEraseBkgnd( IRenderTarget  *pRT)
	{
	    CUIMoveWindow::OnEraseBkgnd( pRT);
	}
}
