﻿#ifndef Units_H_
#define Units_H_
#include <string>
#include <map>
namespace KKUI
{

	template<class TObj,class TKey=std::string>
	class CCmnMap
	{
		public:
		    CCmnMap(void (*funOnKeyRemoved)(const TObj &)=NULL):m_pFunOnKeyRemoved(funOnKeyRemoved)
			{
				m_mapNamedObj=new std::map<TKey,TObj>;
			}

			///判断键值是否存在
			bool HasKey(const TKey & key) const
			{
				std::map<TKey,TObj>::iterator It=m_mapNamedObj->find(key);
				return m_mapNamedObj->find(key)!=m_mapNamedObj->end();
			}
			bool GetKeyObject(const TKey & key,TObj & obj)
			{
				if(!HasKey(key)) 
					return false;
				obj=(*m_mapNamedObj)[key];
				return true;
			}

			TObj & GetKeyObject(const TKey & key) const
			{
				return (*m_mapNamedObj)[key];
			}
			bool AddKeyObject(const TKey & key,const TObj & obj)
			{
				if(HasKey(key)) 
					return false;
				(*m_mapNamedObj)[key]=obj;
				return true;
			}
			void SetKeyObject(const TKey & key,const TObj & obj)
			{
				RemoveKeyObject(key);
				(*m_mapNamedObj)[key]=obj;
			}
			bool RemoveKeyObject(const TKey & key)
			{
				if(!HasKey(key)) 
					return false;
				if(m_pFunOnKeyRemoved)
				{
					m_pFunOnKeyRemoved((*m_mapNamedObj)[key]);
				}
				std::map<TKey,TObj>::iterator It=m_mapNamedObj->find(key);
				m_mapNamedObj->erase(It);
				return true;
			}
			void RemoveAll()
			{
				if(m_pFunOnKeyRemoved)
				{
					std::map<TKey,TObj>::iterator it=m_mapNamedObj->begin();
					while(it!=m_mapNamedObj->end())
					{ 
						m_pFunOnKeyRemoved(it->second);
						++it;
					}
				}
				m_mapNamedObj->RemoveAll();
			}
			int GetCount()
			{
				return m_mapNamedObj->size();
			}
		protected:
			void (*m_pFunOnKeyRemoved)(const TObj &obj);
			std::map<TKey,TObj>*   m_mapNamedObj;
	};
}
#endif