#ifndef KKUI_IRender_H_
#define KKUI_IRender_H_
#include "IObjRef.h"
#include "IkkDraw.h"
#include <string>
namespace KKUI
{

	enum OBJTYPE
    {
        OT_NULL=0,
		OT_FONT,
        OT_BITMAP,
		OT_PICYUV420P,
		OT_RenderTarget,
    };

    enum EXPEND_MODE
	{
		EM_NULL=0,      /*<不变*/
		EM_STRETCH,     /*<拉伸*/
		EM_TILE,        /*<平铺*/
	};

	enum FilterLevel {
		kNone_FilterLevel = 0,
		kLow_FilterLevel,
		kMedium_FilterLevel,
		kHigh_FilterLevel
	};

	 /**
    * @struct     IRenderObj
    * @brief      渲染对象基类
    * 
    * Describe    所有渲染对象全部使用引用计数管理生命周期
    */
    class IRenderObj : public IObjRef 
    {
		public:
			    virtual const OBJTYPE ObjectType() const = 0;

				//由于dx可能丢失设备，这里用来清理资源
				virtual void  Release2() = 0;
				//重新加载资源
				virtual void  ReLoadRes() = 0;
    };

	class  IFont : public IRenderObj
    {
	   public:
		       virtual int TextSize()=0;
			   virtual const OBJTYPE ObjectType() const
			{
				return OT_FONT;
			}
	};
	//位图
    class IBitmap: public IRenderObj
    {
		public:
			virtual const OBJTYPE ObjectType() const
			{
				return OT_BITMAP;
			}
			virtual kkSize    Size() const =0;
			virtual int       Width() = 0;
			virtual int       Height() = 0;
			virtual int       LoadFromFile(const char*FileName)=0;
            virtual int	      LoadFromMemory(const void* pBuf,int szLen) = 0;
	};

	//创建一个YUV420p
	class IPicYuv420p: public IRenderObj
    {
		public:
			virtual const OBJTYPE ObjectType() const
			{
				return OT_PICYUV420P;
			}
			virtual kkSize    Size() const =0;
		    //加载YUV数据
            virtual int	      Load(const kkPicInfo* data) = 0;
	};
	//渲染引擎接口
    class IRenderTarget: public IRenderObj
	{
	   public:
		   virtual const OBJTYPE ObjectType() const{return OT_RenderTarget;}
		     virtual void  ReSize(int nWidth, int nHeight) = 0;
			 //绘制位图
		     virtual int   DrawBitmap(kkRect* pRcDest,IBitmap *pBitmap,int xSrc,int ySrc,int cx,int cy,unsigned char byAlpha=0xFF) = 0;

			 virtual int   DrawBitmapEx(kkRect*  pRcDest,IBitmap *pBitmap,kkRect* pRcSrc,unsigned int expendMode,unsigned char byAlpha=0xFF/**/ ) = 0 ;
             //绘制九宫格
			 virtual int   DrawBitmap9Patch(kkRect* pRcDest,IBitmap *pBitmap,kkRect* pRcSrc,kkRect* pRcSourMargin,unsigned expendMode,unsigned char byAlpha=0xFF) = 0;
			 virtual int   DrawYuv420p(kkRect* pRcDest,IPicYuv420p *pYuv420p,int xSrc,int ySrc,unsigned char byAlpha=0xff,unsigned char R=0xFF,unsigned char G=0xFF,unsigned char B=0xFF) = 0;
			 
			 //绘制线段
			 virtual void  DrawLine(int startx,int starty, int endx,int endy, unsigned int color) =0;
             virtual void  DrawText(IFont* pFont,wchar_t *text,kkRect* pRcDest) =0;
			 virtual void  MeasureText(IFont* pFont,wchar_t *text,kkSize *pSz ) = 0;
			 virtual int   FillRect(kkRect* rt,unsigned int cr)=0;
	};

	class  IRenderFactory : public IObjRef
    {
	   public:
		        virtual bool   init() = 0;
				virtual int    CreateRenderTarget(IRenderTarget ** ppRenderTarget,void* Handle,int nWid,int nHei) =0 ;
				virtual int    CreateBitmap(IBitmap ** ppBitmap) = 0 ;
				virtual int    CreateYUV420p(IPicYuv420p ** ppPicYuv420p) = 0 ;

				virtual void   BeforePaint(IRenderTarget* pRenderTarget) = 0;
				virtual void   EndPaint(IRenderTarget* pRenderTarget) = 0;
				virtual void   CreateFont(IFont** ppFont,int FontSize,wchar_t* FontName) = 0;
	};

}
#endif