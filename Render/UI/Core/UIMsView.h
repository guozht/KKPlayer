#ifndef  KKUI_UIMsView_H_
#define  KKUI_UIMsView_H_
#include "UIMoveState.h"
#include "UIScrollView.h"
#include "UIContainerImpl.h"
#include "UISkin.h"
#include <vector>
//多媒体操作试图,
namespace KKUI
{
	class CUIMsView: public CUIScrollView
	{
	    KKUI_CLASS_NAME(CUIScrollView,CUIMsView,"uimsview")
	    public:
			  CUIMsView();
			  ~CUIMsView();
			  void       InsertChild(CUIWindow *pNewChild,CUIWindow *pInsertAfter=UICWND_LAST);
			  void       DeleteStage(CUIMoveState *Child);
			  bool       DestroyChild(CUIWindow  *pChild);
	    protected:
			  void       OnSize(unsigned int nType, int x,int y);
			  void       OnEraseBkgnd( IRenderTarget  *pRT);
			  KKUI_MSG_MAP_BEGIN()   
					MSG_UI_SIZE(OnSize)
					MSG_UI_ERASEBKGND(OnEraseBkgnd)
			  KKUI_MSG_MAP_END(CUIScrollView)
	   protected:
		     CSize      GetScrollPos();
		     void       UpdateChildrenPosition();
			 void       OnScrollPosChanged();
	   private:
		     //舞台个数
		     int                         m_nStageCount;
			 std::vector<CUIMoveState*>  m_StageVec;
			 //舞台控件位置
			 CUIWindow*                  m_pFirstStageChild;

	};
}
#endif