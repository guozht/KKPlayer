#include "UIMsView.h"

namespace KKUI
{
	CUIMsView::CUIMsView():m_nStageCount(0)
	{
		
	}
	CUIMsView::~CUIMsView()
	{
	
	}

	void  CUIMsView::InsertChild(CUIWindow *pNewChild,CUIWindow *pInsertAfter)
	{
		char* classnmae = pNewChild->GetObjectClass();
		if (strcmp(classnmae, "uimovestate") == 0) {
			CUIScrollView::InsertChild(pNewChild, pInsertAfter);
			m_nStageCount++;
			m_StageVec.push_back((CUIMoveState*)pNewChild);
		}
		else {
			CUIScrollView::InsertChild(pNewChild,  UICWND_FIRST);
		}
		
	}
	 bool     CUIMsView::DestroyChild(CUIWindow  *pChild)
	 {
            DeleteStage((CUIMoveState*)pChild);
		    std::vector<CUIMoveState*>::iterator It=  m_StageVec.begin();
			for(;It!= m_StageVec.end();++It){
					  CUIMoveState* Stage=*It;
					  Stage->DelIntersectCtl(pChild);
			}
		   return CUIScrollView::DestroyChild(pChild);
	 }
	void       CUIMsView::DeleteStage(CUIMoveState *Child)
	{
	      std::vector<CUIMoveState*>::iterator It=  m_StageVec.begin();
			for(;It!= m_StageVec.end();++It)
			{
					  CUIMoveState* Stage=*It;
					  if(Stage==Child){
						  m_StageVec.erase(It);
						  break;
					  }

			}
	}
	void  CUIMsView::OnSize(unsigned int nType, int x,int y)
	 {
		    CSize size;
		    size.cx = x;
		    size.cy = y;
        
			
			CUIScrollView::OnSize(nType,x,y);
			UpdateScrollBar();
			UpdateChildrenPosition();
			
	}
	void CUIMsView::OnEraseBkgnd( IRenderTarget  *pRT)
	{
		CUIWindow::OnEraseBkgnd(pRT);
	}
    CSize  CUIMsView::GetScrollPos()
	{
		CSize ss(m_siHoz.nPos,m_siVer.nPos);
		return ss;
	}
	void CUIMsView::UpdateChildrenPosition()
	{
		   
			CRect rt;
			this->GetClientRect(&rt);
			CUIWindow *pChild=GetWindow(GSW_FIRSTCHILD);
			CSize ss=GetScrollPos();
			while(pChild)
			{
				if(pChild->IsFloat())
				{
					pChild->OnUpdateFloatPosition( ss,rt);
					pChild->UpdateChildrenPosition();
				}
				pChild=pChild->GetWindow(GSW_NEXTSIBLING);
			}

			CalViewSz();
			UpdateScrollBar();
            OnNcCalcSize(0,0);

            //开始计算舞台控件包含的控件
			std::vector<CUIMoveState*>::iterator It=  m_StageVec.begin();
			for(;It!= m_StageVec.end();++It){
					  CUIMoveState* Stage=*It;
					  Stage->ClearIntersectCtl();
					  CRect StageRt;
					  Stage->GetWindowRect(&StageRt);

					  //遍历所有的子窗口
				      CUIWindow *pChild=GetWindow(GSW_FIRSTCHILD);
					  while(pChild){

						  char* classnmae = pChild->GetObjectClass();
						  if (strcmp(classnmae, "uimovestate") != 0) { 
							  CRect Rt;
						      pChild->GetWindowRect(&Rt);
							  if(kkRectIntersects(StageRt,Rt)){
								  Stage->AddIntersectCtl(pChild);
							  }
						   }
						  
						   pChild=pChild->GetWindow(GSW_NEXTSIBLING);
					   }
			}
	}
	void CUIMsView::OnScrollPosChanged()
	{
	        CRect rt;
			this->GetClientRect(&rt);
			CUIWindow *pChild=GetWindow(GSW_FIRSTCHILD);
			 CSize ss=GetScrollPos();
			while(pChild)
			{
				if(pChild->IsFloat())
				{
					pChild->OnUpdateFloatPosition( ss,rt);
					pChild->UpdateChildrenPosition();
				}
				pChild=pChild->GetWindow(GSW_NEXTSIBLING);
			}
	}
}