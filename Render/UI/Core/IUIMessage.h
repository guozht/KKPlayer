#ifndef  KKUI_IUIMessage_H_
#define  KKUI_IUIMessage_H_
namespace KKUI
{

	enum UIEventType
	{
	    E_Click     = 0,
		E_RClick    = 1,
		E_CtlSize   = 2,
		E_CltMove   = 3,
	};
	struct UIEvent
	{
	    void*         Handle;
		UIEventType   type;
		unsigned int  pa1;
		unsigned int  pa2;
	};
    class IUIMessage
	{
	   public:
		      virtual int     UISendMessage(unsigned int Msg, unsigned int wParam, unsigned long lParam) =0;
			  virtual void    UIInvalidateRect(int l,int r,int t,int b) = 0;
			  virtual void    UIInvalidate() =0 ;
			  virtual void    UISetCursor(const char* name) =0;
              virtual void    OnUIEvent(const UIEvent* ev) =0;
	};
}
#endif