#ifndef UIEvent_H_
#define UIEvent_H_
namespace KKUI{
	class CUIBase;
	typedef struct tagUITEvent
	{
		int           Type;
		CUIBase*      pSender;
		long long     dwTimestamp;
		unsigned int  wParam;
		unsigned int  lParam;
	} UITEvent;
}
#endif