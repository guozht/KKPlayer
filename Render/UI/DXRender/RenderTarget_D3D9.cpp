//#include <d3dx9tex.h>
#include <wchar.h>
#include <stdio.h>
#include <assert.h>
#include <string>

#include <d3dx9math.h>

#include <core\SkCanvas.h>
#include <core\SkBitmap.h>
#include <core\SkTypeface.h>
#include <core\SkImageDecoder.h>
#include <core\SkStream.h>

#include "RenderTarget_D3D9.h"
#include "Yuv420p_D3D9.h"
#ifndef SAFE_RELEASE
#define SAFE_RELEASE(p)      { if (p) { (p)->Release(); (p)=NULL; } }
#endif

typedef IDirect3D9* (WINAPI* LPDIRECT3DCREATE9)( UINT );

typedef HRESULT (WINAPI *DX9_D3DXCreateSprite)( LPDIRECT3DDEVICE9   pDevice, LPD3DXSPRITE*       ppSprite);

typedef HRESULT (WINAPI *DX9_DX9CTFromFileInMemory)(LPDIRECT3DDEVICE9 pDevice,LPCVOID pSrcData,UINT  SrcDataSize,LPDIRECT3DTEXTURE9*  ppTexture);
typedef HRESULT (WINAPI *DX9_D3DXCreateTextureFromFile)( LPDIRECT3DDEVICE9  pDevice,LPCTSTR            pSrcFile,LPDIRECT3DTEXTURE9 *ppTexture);

//x
typedef D3DXMATRIX* (WINAPI *DX9_D3DXMatrixRotationY)(D3DXMATRIX *pOut, FLOAT Angle );
typedef D3DXMATRIX* (WINAPI *DX9_D3DXMatrixRotationZ)(D3DXMATRIX *pOut, FLOAT Angle );

typedef D3DXMATRIX* (WINAPI *DX9_D3DXMatrixScaling)( D3DXMATRIX *pOut, FLOAT sx, FLOAT sy, FLOAT sz );
typedef D3DXMATRIX* (WINAPI *DX9_D3DXMatrixTranslation)( D3DXMATRIX *pOut, FLOAT x, FLOAT y, FLOAT z );
typedef D3DXMATRIX* (WINAPI *DX9_D3DXMatrixMultiply)( D3DXMATRIX *pOut, CONST D3DXMATRIX *pM1, CONST D3DXMATRIX *pM2 );
typedef D3DXMATRIX* (WINAPI *DX9_D3DXMatrixLookAtLH)( D3DXMATRIX *pOut, CONST D3DXVECTOR3 *pEye, CONST D3DXVECTOR3 *pAt,CONST D3DXVECTOR3 *pUp );
typedef D3DXMATRIX* (WINAPI *DX9_D3DXMatrixPerspectiveFovLH)( D3DXMATRIX *pOut, FLOAT fovy, FLOAT Aspect, FLOAT zn, FLOAT zf );
typedef D3DXMATRIX* (WINAPI *DX9_D3DXMatrixOrthoLH)( D3DXMATRIX *pOut, FLOAT w, FLOAT h, FLOAT zn, FLOAT zf );
typedef HRESULT  (WINAPI *DX9_D3DXCreateFontIndirect)( LPDIRECT3DDEVICE9       pDevice, CONST D3DXFONT_DESCW*   pDesc, LPD3DXFONT*             ppFont);

static DX9_D3DXMatrixOrthoLH                  fpDX9_D3DXMatrixOrthoLH     = NULL;
static DX9_D3DXMatrixTranslation              fpDX9_D3DXMatrixTranslation = NULL;
static DX9_D3DXMatrixScaling                  fpDX9_D3DXMatrixScaling     = NULL;
static DX9_D3DXMatrixRotationZ                fpDX9_D3DXMatrixRotationZ   = NULL;
static DX9_D3DXMatrixRotationY                fpDX9_D3DXMatrixRotationY   = NULL;
static DX9_D3DXCreateTextureFromFile          fpD3DXCreateTextureFromFile = NULL;
static DX9_DX9CTFromFileInMemory              fpDX9CTFromFileInMemory     = NULL;
static DX9_D3DXMatrixMultiply                 fpDX9_D3DXMatrixMultiply    = NULL;
static DX9_D3DXMatrixLookAtLH                 fpDX9_D3DXMatrixLookAtLH    = NULL;
static DX9_D3DXMatrixPerspectiveFovLH         fpDX9_D3DXMatrixPerspectiveFovLH =NULL;
static DX9_D3DXCreateSprite                   fpDX9_D3DXCreateSprite      = NULL;
static DX9_D3DXCreateFontIndirect             fpDX9_D3DXCreateFontIndirect = NULL;
static LPDIRECT3DCREATE9                      pfnDirect3DCreate9          = NULL;

static  HMODULE hModD3D9  = NULL;
static  HMODULE hD3dx9_43 = NULL;

LPCSTR GetUTF8String(LPCWSTR str);
extern "C"
{
    D3DXMATRIX* WINAPI D3DXMatrixMultiply
    ( D3DXMATRIX *pOut, CONST D3DXMATRIX *pM1, CONST D3DXMATRIX *pM2 )
	{
	          return fpDX9_D3DXMatrixMultiply(pOut ,pM1, pM2);
	}
}

 void charTowchar2(const char *chr, wchar_t *wchar, int size)  
      {     
         MultiByteToWideChar( CP_ACP, 0, chr,  
         strlen(chr)+1, wchar, size/sizeof(wchar[0]) );  
      }

//#define USE_RHW
namespace KKUI
{
	///向量数据
	struct KKUI_Vertex
	{
		float x, y, z;
		#ifdef USE_RHW
			float  w;
        #endif
		DWORD diffuse;
		float u, v;

		enum 
		{
			//FVF = D3DFVF_XYZRHW|D3DFVF_DIFFUSE|D3DFVF_TEX1,
		//	FVF = D3DFVF_DIFFUSE|D3DFVF_TEX1
			#ifdef USE_RHW
					FVF = D3DFVF_DIFFUSE|D3DFVF_TEX1|D3DFVF_XYZRHW,
            #else
			     FVF =D3DFVF_XYZ | D3DFVF_TEX1|D3DFVF_DIFFUSE ,
			#endif
		};
	};

	struct KKUI_2DVertex
	{
		float x, y, z;
		
			float  w;
     
		DWORD diffuse;
		float u, v;

		enum 
		{
		
					FVF =D3DFVF_DIFFUSE|D3DFVF_TEX1|D3DFVF_XYZRHW,

		};
	};

	#ifdef USE_RHW
	void GetUIVertex(KKUI_Vertex* vec,const kkRect& rt,int xSrc,int ySrc,DWORD diffuse,int textWidth,int textHeight,int SWdith,int SHeight)
	{
		int w=rt.right-rt.left;
		int h=rt.bottom -rt.top;
	
		vec[0].x =rt.left ;  //A     A(leftTop)->B(rightTop)->C(leftBu)->D->(rightBu)
		vec[0].y =rt.top;
		vec[0].z = 0.0f;
		vec[0].w = 1.f;
		vec[0].u = (float)xSrc/textWidth;
		vec[0].v = (float)ySrc/textHeight;
		vec[0].diffuse=diffuse;

		vec[1].x =  rt.right;  //B
		vec[1].y =  rt.top;
		vec[1].z = 0.0f;
		vec[1].w = 1.f;
		vec[1].u = 1.f;
		vec[1].v =(float)ySrc/textHeight;
        vec[1].diffuse=diffuse;

		vec[2].x =rt.left;   //C
		vec[2].y =rt.bottom ;
		vec[2].z = 0.0f;
		vec[2].w = 1.0f;
		vec[2].u = (float)xSrc/textWidth;
		vec[2].v = 1.0f;
        vec[2].diffuse=diffuse;

		vec[3].x =rt.right ; //D
		vec[3].y =rt.bottom;
		vec[3].z = 0.0f;
		vec[3].w = 1.0f;
		vec[3].u = 1.0f;
		vec[3].v = 1.0f;
		vec[3].diffuse=diffuse;
	}
    #else
	    
	    void SetCoordinate(float ptx,float pty,float &OutPtx,float &OutPty,int SWdith,int SHeight)   //窗口坐标转换成屏幕坐标(十字坐标)  
		{  
			POINT Cpt;             //原点  
			Cpt.x=SWdith/2;        //必要的话,可以用::GetClientRect()函数来获得窗口宽、高  
			Cpt.y=SHeight/2;  
			if(ptx>Cpt.x && pty>Cpt.y)      //第一象限坐标转换  
			{  
				ptx-=Cpt.x;  
				pty=(pty-Cpt.y)*-1.0f;  
			}  
			else if(ptx<Cpt.x && pty>Cpt.y) //第二象限坐标转换  
			{  
				ptx-=Cpt.x;  
				pty=(pty-Cpt.y)*-1.0f;  
			}  
			else if(ptx<Cpt.x && pty<Cpt.y) //第三象限坐标转换  
			{  
				ptx-=Cpt.x;  
				pty=(pty-Cpt.y)*-1.0f;  
			}  
			else if(ptx>Cpt.x && pty<Cpt.y) //第四象限坐标转换  
			{  
				ptx-=Cpt.x;  
				pty=(pty-Cpt.y)*-1.0f;  
			}  
			else if(ptx==Cpt.x && pty<Cpt.y)    //点在Y正半轴上:补充判断  
			{  
				ptx=0.0f;  
				pty=(pty-Cpt.y)*-1.0f;  
			}  
			else if(ptx==Cpt.x && pty>Cpt.y)    //点在Y负半轴上:补充判断  
			{  
				ptx=0.0f;  
				pty=(pty-Cpt.y)*-1.0f;  
			}  
			else if(ptx>Cpt.x && pty==Cpt.y)    //点在X正半轴上:补充判断  
			{  
				ptx-=Cpt.x;  
				pty=0.0f;  
			}  
			else if(ptx<Cpt.x && pty==Cpt.y)    //点在X负半轴上:补充判断  
			{  
				ptx-=Cpt.x;  
				pty=0.0f;  
			}  
			else if(ptx==Cpt.x && pty==Cpt.y)                               //原点(0,0)  
			{  
				ptx-=Cpt.x;  
				ptx-=Cpt.y;  
				//ptx=0.0f;  
				//pty=0.0f;  
			}  
			OutPtx=ptx;  
			OutPty=pty;  
		}    

		void GetUIVertex(KKUI_Vertex* vec,const kkRect& rt,int xSrc,int ySrc,DWORD diffuse,int textWidth,int textHeight,int SWdith,int SHeight)
		{
		
			//A  leftTop
			SetCoordinate(rt.left,rt.top,  vec[0].x,vec[0].y ,SWdith,SHeight);
			vec[0].z = 0.0f;
			vec[0].u =(float)xSrc/textWidth;
			vec[0].v = (float)ySrc/textHeight;
			vec[0].diffuse=diffuse;

			//B  rightTop
			SetCoordinate(rt.right,rt.top,  vec[1].x,vec[1].y ,SWdith,SHeight);
			vec[1].z = 0.0f;
			vec[1].u = 1.0f;
			vec[1].v = (float)ySrc/textHeight;
			vec[1].diffuse=diffuse;

			
            //C
			SetCoordinate(rt.left,rt.bottom,  vec[2].x,vec[2].y ,SWdith,SHeight);
			vec[2].z = 0.0f;
			//vec[2].w = 1.0f;
			vec[2].u = (float)xSrc/textWidth;
			vec[2].v = 1.0f;
			vec[2].diffuse=diffuse;

			 //D
			SetCoordinate(rt.right,rt.bottom,  vec[3].x,vec[3].y ,SWdith,SHeight);
			vec[3].z = 0.0f;
			vec[3].u = 1.0f;
			vec[3].v = 1.0f;
			vec[3].diffuse=diffuse;
		}
    #endif

	//用于2d
	void GetUIRHWVertex(KKUI_2DVertex* vec,const kkRect& rt,DWORD diffuse,int xSrc,int ySrc,int NeedWidth,int NeedHeight,int SWidth,int SHeight)
	{
		int w=rt.right-rt.left;
		int h=rt.bottom -rt.top;
	
		vec[0].x =rt.left ;  //A     A(leftTop)->B(rightTop)->C(leftBu)->D->(rightBu)
		vec[0].y =rt.top;
		vec[0].z = 0.0f;
		vec[0].w = 1.f;
		vec[0].u = (float)xSrc/SWidth;  //x
		vec[0].v = (float)ySrc/SHeight; //y
		vec[0].diffuse=diffuse;

		vec[1].x =  rt.right;  //B
		vec[1].y =  rt.top;
		vec[1].z = 0.0f;
		vec[1].w = 1.f;
		vec[1].u = (float)(xSrc+ NeedWidth)/ SWidth;
		vec[1].v =(float)ySrc/SHeight;
        vec[1].diffuse=diffuse;

		vec[2].x =rt.left;   //C
		vec[2].y =rt.bottom ;
		vec[2].z = 0.0f;
		vec[2].w = 1.0f;
		vec[2].u = (float)xSrc/SWidth;
		vec[2].v = (float)(ySrc+NeedHeight)/SHeight;
        vec[2].diffuse=diffuse;

		vec[3].x =rt.right ; //D
		vec[3].y =rt.bottom;
		vec[3].z = 0.0f;
		vec[3].w = 1.0f;
		vec[3].u = (float)(xSrc+ NeedWidth)/ SWidth;
		vec[3].v = (float)(ySrc+NeedHeight)/SHeight;
		vec[3].diffuse=diffuse;
	}
    static D3DPRESENT_PARAMETERS GetPresentParams(HWND hView)
	{
		D3DPRESENT_PARAMETERS PresentParams;
		ZeroMemory(&PresentParams, sizeof(PresentParams));
		PresentParams.BackBufferFormat =D3DFMT_UNKNOWN;
		//D3DFMT_A8R8G8B8; 
		PresentParams.BackBufferCount=0;
		PresentParams.MultiSampleType = D3DMULTISAMPLE_NONE;
		PresentParams.MultiSampleQuality = 0;
		PresentParams.SwapEffect = D3DSWAPEFFECT_DISCARD;
		PresentParams.hDeviceWindow = hView;
		PresentParams.Windowed = TRUE;
		PresentParams.EnableAutoDepthStencil = TRUE;
		PresentParams.AutoDepthStencilFormat = D3DFMT_D24X8;/**/
		PresentParams.Flags = D3DPRESENTFLAG_VIDEO;
		PresentParams.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
		PresentParams.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;
		return PresentParams; 
    }

	SRenderFactory_D3D9::SRenderFactory_D3D9():
		m_pDevice(0),
		m_pD3D(0)
	{
	    unsigned int xxx=D3DCOLOR_ARGB(0xFF,0xFA,0xFB,0xFC) ;
			xxx++;
	}
	SRenderFactory_D3D9::~SRenderFactory_D3D9()
	{
		//SAFE_RELEASE(m_pSprite);
	    SAFE_RELEASE(m_pDevice);
		SAFE_RELEASE(m_pD3D);
	}
	bool SRenderFactory_D3D9::init()
	{
	    if(hModD3D9==0)
			 hModD3D9 = LoadLibraryA("d3d9.dll");
		
		if (hModD3D9)
		{
			pfnDirect3DCreate9 = (LPDIRECT3DCREATE9)GetProcAddress(hModD3D9, "Direct3DCreate9");
			if(hD3dx9_43==NULL)
				hD3dx9_43 = LoadLibraryA("D3dx9_43.dll");
			if(hD3dx9_43)
			{
				 fpDX9_D3DXCreateFontIndirect = (DX9_D3DXCreateFontIndirect)      GetProcAddress(hD3dx9_43, "D3DXCreateFontIndirectW");
				 fpDX9_D3DXCreateSprite       = (DX9_D3DXCreateSprite)      GetProcAddress(hD3dx9_43, "D3DXCreateSprite");
				 fpDX9CTFromFileInMemory      = (DX9_DX9CTFromFileInMemory)      GetProcAddress(hD3dx9_43, "D3DXCreateTextureFromFileInMemory");
				 fpD3DXCreateTextureFromFile  = (DX9_D3DXCreateTextureFromFile)  GetProcAddress(hD3dx9_43, "D3DXCreateTextureFromFile");
				 fpDX9_D3DXMatrixRotationY    = (DX9_D3DXMatrixRotationY)    GetProcAddress(hD3dx9_43, "D3DXMatrixRotationY");
				 fpDX9_D3DXMatrixRotationZ    = (DX9_D3DXMatrixRotationZ)    GetProcAddress(hD3dx9_43, "D3DXMatrixRotationZ");

				 fpDX9_D3DXMatrixTranslation  = (DX9_D3DXMatrixTranslation)    GetProcAddress(hD3dx9_43, "D3DXMatrixTranslation");
                 fpDX9_D3DXMatrixScaling      = (DX9_D3DXMatrixScaling)    GetProcAddress(hD3dx9_43, "D3DXMatrixScaling");
				 fpDX9_D3DXMatrixMultiply     = (DX9_D3DXMatrixMultiply  )    GetProcAddress(hD3dx9_43, "D3DXMatrixMultiply");
				 fpDX9_D3DXMatrixLookAtLH     = (DX9_D3DXMatrixLookAtLH  )    GetProcAddress(hD3dx9_43, "D3DXMatrixLookAtLH");
				 fpDX9_D3DXMatrixPerspectiveFovLH = (DX9_D3DXMatrixPerspectiveFovLH  )    GetProcAddress(hD3dx9_43, "D3DXMatrixPerspectiveFovLH");
				 fpDX9_D3DXMatrixOrthoLH      = (DX9_D3DXMatrixOrthoLH )    GetProcAddress(hD3dx9_43, "D3DXMatrixOrthoLH");
				 
			}else{
				/* std::wstring path=GetModulePath();
				 path+=L"\\dx\\D3dx9_43.dll";
				 hD3dx9_43 = LoadLibrary(path.c_str());
				 if(hD3dx9_43)
				 {
					  fpDX9CTFromFileInMemory= (DX9CTFromFileInMemory)GetProcAddress(hD3dx9_43, "D3DXCreateTextureFromFileInMemory");
				 }*/
			}
			
		}else{
			return false;
		}

		if(pfnDirect3DCreate9==NULL)
		{
		
			return false;
		}
		m_pD3D=pfnDirect3DCreate9(D3D_SDK_VERSION);

		DWORD BehaviorFlags =D3DCREATE_FPU_PRESERVE | D3DCREATE_PUREDEVICE | D3DCREATE_HARDWARE_VERTEXPROCESSING|D3DCREATE_NOWINDOWCHANGES;

		HWND hView = GetShellWindow();
		D3DPRESENT_PARAMETERS PresentParams = GetPresentParams(hView);
       
		HRESULT hr = m_pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL,hView, BehaviorFlags, &PresentParams, &m_pDevice);
	   
		if (FAILED(hr) && hr != D3DERR_DEVICELOST)
		{

			return false;
		}

		//fpDX9_D3DXCreateSprite(m_pD3D,&m_pSprite);
		return 1;
	}
	int  SRenderFactory_D3D9::CreateRenderTarget(IRenderTarget ** ppRenderTarget,void* Handle,int nWid,int nHei)
	{
		Lock();
		CRenderTarget_D3D9* re = new CRenderTarget_D3D9(this,(HWND)Handle,nWid,nHei);
		*ppRenderTarget = re;
		m_IRenderObjSet.insert((IRenderObj*)re);
		UnLock();
	   return 0;
	}


	void         SRenderFactory_D3D9::RemoveObj(IRenderObj* obj)
	{
		Lock();
		std::set<IRenderObj*>::iterator It= m_IRenderObjSet.find(obj);
		if(It != m_IRenderObjSet.end())
			m_IRenderObjSet.erase(It);
		UnLock();
	}

bool  SRenderFactory_D3D9::LostDevice()
{
	
    HRESULT hr = m_pDevice->TestCooperativeLevel();
    if (hr == D3DERR_DEVICELOST)
    {
		return true;
       
    }else if (hr == D3DERR_DEVICENOTRESET)
    { 
		return true;
    }


    return false;
}

bool     SRenderFactory_D3D9::LostDeviceRestore()
{
    HRESULT hr = m_pDevice->TestCooperativeLevel();
    if (hr == D3DERR_DEVICELOST)
    {
		return true;
       
    }else if (hr == D3DERR_DEVICENOTRESET)
    { 
			   std::set<IRenderObj*>::iterator It= m_IRenderObjSet.begin();
				for(;It!= m_IRenderObjSet.end();++It){
				   (*It)->Release2();
				}
				HWND hView = GetShellWindow();
				D3DPRESENT_PARAMETERS PresentParams = GetPresentParams(hView);
				hr = m_pDevice->Reset(&PresentParams);
				if(FAILED(hr))
				{
					return false;
				}

				It= m_IRenderObjSet.begin();
				for(;It!= m_IRenderObjSet.end();++It){
				   (*It)->ReLoadRes();
				}

				return true;
    }


    return false;
}
    int    SRenderFactory_D3D9::CreateBitmap(IBitmap ** ppBitmap)
    {
		Lock();
		CBitmap_D3D9 *p = new CBitmap_D3D9(this);
		UnLock();
        *ppBitmap = p;
		m_IRenderObjSet.insert(p);
       return 0;
    }

	int    SRenderFactory_D3D9::CreateYUV420p(IPicYuv420p ** ppBitmap)
	{
		     Lock();
              CYuv420p_D3D9* pD = new CYuv420p_D3D9(this);
			  *ppBitmap=pD;
			  m_IRenderObjSet.insert(pD);
		      UnLock();
	        return 0;
	}
	void   SRenderFactory_D3D9::BeforePaint(IRenderTarget* pRenderTarget)
	{
	     HRESULT hr=0;
		 Lock();
		 if(LostDeviceRestore()){
		    return;
		 }
		 CRenderTarget_D3D9* pRenderTargetDx=(CRenderTarget_D3D9*)pRenderTarget;
		 IDirect3DSurface9  *pSurface=pRenderTargetDx->GetSurface();
	     hr =m_pDevice->SetRenderTarget(0,pSurface);
		 pSurface->Release(); 
		 
		 
	     m_pDevice->Clear(0, NULL, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);
         hr =m_pDevice->BeginScene();
		
		 //创建一个左手坐标系正交投影矩阵
		 D3DXMATRIX Ortho2D;
         D3DXMATRIX Identity;
		 fpDX9_D3DXMatrixOrthoLH(&Ortho2D, pRenderTargetDx->GetWidth(),pRenderTargetDx->GetHeight(), 0.0f, 1.0f);
		 D3DXMatrixIdentity(&Identity);

		 m_pDevice->SetTransform(D3DTS_PROJECTION, &Ortho2D);
		 m_pDevice->SetTransform(D3DTS_WORLD, &Identity);
		 m_pDevice->SetTransform(D3DTS_VIEW, &Identity);/**/

		// m_pDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);
		 //  /* D3DXVECTOR3 vEyePt( 0.0f,0.0f,-1.0f );
   //         D3DXVECTOR3 vLookatPt( 0.0f, 0.0f, 0.0f );
   //         D3DXVECTOR3 vUpVec( 0.0f, 1.0f, 0.0f );*/

		 //   //眼睛的位置
		 //   D3DXVECTOR3 vEyePt( 0.0f, 0.0f, -1.0f ); 
			//
			////摄像机观察目标位置向量
			//D3DXVECTOR3 vLookatPt ( 0.0f, 0.0f, 0.0f );

			////当前世界坐标系向上方向向量
			//D3DXVECTOR3 vUpVec ( 0.0f, 1.0f, 0.0f );

   //         D3DXMATRIXA16 matView;
   //         fpDX9_D3DXMatrixLookAtLH( &matView, &vEyePt, &vLookatPt, &vUpVec );
   //         m_pDevice->SetTransform( D3DTS_VIEW, &matView );

			//

			////https://www.cnblogs.com/markuya/articles/1517348.html
			////这个变换矩阵是投影变换的预变换。
			//// 定义投影矩阵很像是定义摄像机的镜头
			//D3DXMATRIXA16 matProj;
   //         fpDX9_D3DXMatrixPerspectiveFovLH( &matProj, D3DX_PI / 4, 1.0f, 0.0f, 100.0f );
   //         m_pDevice->SetTransform( D3DTS_PROJECTION, &matProj );

		
	}
	void   SRenderFactory_D3D9::EndPaint(IRenderTarget* pRenderTarget)
	{

		if(LostDeviceRestore()){
			UnLock();
		    return;
		 }

	     HRESULT hr=0;
		 CRenderTarget_D3D9* pRenderTargetDx=(CRenderTarget_D3D9*)pRenderTarget;
		 m_pDevice->EndScene();

		 IDirect3DSwapChain9* pSwapChain= pRenderTargetDx->GetSwapChain();
		 hr=pSwapChain->Present(NULL, NULL, NULL,NULL , NULL);
		 UnLock();
	}
	void   SRenderFactory_D3D9::CreateFont(IFont** ppFont,int FontSize,wchar_t* FontName)
	{
		  Lock();
			CFont_D3D9* ff = new CFont_D3D9(this,FontSize,FontName);
			*ppFont=ff;
         m_IRenderObjSet.insert(ff);
         UnLock();
	}
	void   SRenderFactory_D3D9:: Lock()
	{
		  m_lock.Lock();
	}
	void   SRenderFactory_D3D9::UnLock()
	{
		  m_lock.Unlock();
	}



	CFont_D3D9::CFont_D3D9(SRenderFactory_D3D9* pRenderFactory,int FontSize,wchar_t* FontName):m_pD3DXFont(0), 
		m_nTextSize(FontSize),
		m_pRenderFactory(pRenderFactory)
	{
		memset(m_nFontName,0,128);
		if(FontName)
		    wcscpy( m_nFontName,FontName);
	         
		 m_pRenderFactory->AddRef();
		ReLoadRes();	   
					
	}
	CFont_D3D9::~CFont_D3D9()
	{
	   m_pRenderFactory->Release();
	   Release2();
	   m_pRenderFactory->RemoveObj(this);
	}
	void  CFont_D3D9::Release2()
	{
	    SAFE_RELEASE(m_pD3DXFont);
	}
	void CFont_D3D9::ReLoadRes()
	{

		    IDirect3DDevice9* pDevice=m_pRenderFactory->GetDevice();
			D3DXFONT_DESC df;
			ZeroMemory(&df, sizeof(D3DXFONT_DESC));
			df.Height = m_nTextSize;
			//df.Width = 12;
			df.MipLevels = D3DX_DEFAULT;
			df.Italic = false;
			df.CharSet = DEFAULT_CHARSET;
			df.OutputPrecision = 0;
			df.Quality = 0;
			df.PitchAndFamily = 0;
			if(m_nFontName){
                wcscpy(df.FaceName, m_nFontName);
				
			}
			//创建ID3DXFont对象
			fpDX9_D3DXCreateFontIndirect(pDevice, &df, &m_pD3DXFont); 
	}
	int  CFont_D3D9::TextSize()
	{
	   return 0;
	}
	ID3DXFont*    CFont_D3D9::GetD3DXFont()
	{
		return m_pD3DXFont;
	}
	

	 
	 CBitmap_D3D9::CBitmap_D3D9(SRenderFactory_D3D9* pRenderFactory):
	        m_pTexture(0),
			m_pRenderFactory( pRenderFactory),
			m_pSrcBuffer(0),
			m_pSrcBufferLen(0)
	{
	   m_pRenderFactory->AddRef();
	   memset(&m_sz,0,sizeof(m_sz));
	}

	 CBitmap_D3D9::~CBitmap_D3D9()
	 {
	     m_pRenderFactory->Release();
		 Release2();
		 if(m_pSrcBuffer)
			free(m_pSrcBuffer);
		 m_pSrcBuffer =0;
		  m_pRenderFactory->RemoveObj(this);
	 }
	 void   CBitmap_D3D9::Release2()
	 {
	      SAFE_RELEASE(m_pTexture);
	 }
	 void   CBitmap_D3D9::ReLoadRes()
	 {
		    if(!m_pSrcBuffer)
			 return;

	        SAFE_RELEASE(m_pTexture);
			if(fpDX9CTFromFileInMemory!=NULL)
			{
				fpDX9CTFromFileInMemory(m_pRenderFactory->GetDevice(),m_pSrcBuffer,  m_pSrcBufferLen, &m_pTexture);
				
			}
	 }
	 kkSize    CBitmap_D3D9::Size() const
	 {
	    return m_sz;
	 }

     int       CBitmap_D3D9::Width()
	 {
		 return m_sz.cx;
	 }
	 int       CBitmap_D3D9::Height()
	 {
	     return m_sz.cy;
	 }
	 static void charTowchar(const char *chr, wchar_t *wchar, int size)  
     {     
         MultiByteToWideChar( CP_ACP, 0, chr,  
         strlen(chr)+1, wchar, size/sizeof(wchar[0]) );  
     }
    int   CBitmap_D3D9::LoadFromFile(const char*FileName)
	{
		    SAFE_RELEASE(m_pTexture);
			if( fpD3DXCreateTextureFromFile!=NULL&&FileName!=0)
			{
				wchar_t path[512]={};
				charTowchar(FileName,path,512);
				fpD3DXCreateTextureFromFile(m_pRenderFactory->GetDevice(),path, &m_pTexture);
				if(!m_pTexture)
					return 1;

				D3DSURFACE_DESC desc;
			    m_pTexture->GetLevelDesc(0,&desc);
			    m_sz.cx = desc.Width;
			    m_sz.cy = desc.Height;
				return 1;
			}
			return  0;

			
	}
    int	CBitmap_D3D9::LoadFromMemory(const void* pBuf,int szLen)
	{
	        SAFE_RELEASE(m_pTexture);

			SkBitmap Bkbitmap;
		    SkMemoryStream stream(pBuf,szLen,true);
		    SkImageDecoder::DecodeStream(&stream, &Bkbitmap);
			m_sz.cx = Bkbitmap.width();
			m_sz.cy = Bkbitmap.height();
			if(fpDX9CTFromFileInMemory!=NULL)
			{
				fpDX9CTFromFileInMemory(m_pRenderFactory->GetDevice(),pBuf, szLen, &m_pTexture);
				if(!m_pTexture)
					return 0;

				if(m_pSrcBuffer)
					free(m_pSrcBuffer);
				//为了防止设备丢失，暂且先缓存数据
				m_pSrcBuffer = (char*)::malloc(szLen);
				memcpy(m_pSrcBuffer, pBuf,szLen);
			    m_pSrcBufferLen =szLen;
				return 1;
			}
			return  0;
	}

	//https://blog.csdn.net/jujueduoluo/article/details/52926689
	//https://www.cnblogs.com/setoutsoft/p/4086051.html
//	int CBitmap_D3D9::LoadText(const char* m_LeftStr,int len,int ff)
//	{
//		       int ret = 0;
//		        const wchar_t *txt = (const wchar_t *)m_LeftStr;
//				HDC hDc = ::CreateCompatibleDC(NULL);                    // 通过当前桌面创建设备内容HDC
//
//				::SetBkMode(hDc, TRANSPARENT);
//				SetTextColor(hDc, RGB(255, 255, 255));                    // 设置背景颜色和文字的颜色
//			
//			    
//				LOGFONT lf;
//				ZeroMemory(&lf, sizeof(LOGFONT));
//				lf.lfHeight = 12;
//				wcscpy(lf.lfFaceName,L"宋体");
//				HFONT   hFont = CreateFontIndirect(&lf);              // 创建大小为72的字体
//			 
//				SelectObject(hDc, hFont);
//				SIZE sz;
//				GetTextExtentPoint32(hDc, txt, lstrlen(txt), &sz);
//				m_sz.cx =sz.cx;
//				m_sz.cy =sz.cy;
//
//				int width  = m_sz.cx;
//				int height = m_sz.cy;
//
//				char* pBitmapBits;                                                        //  创建一个位图
//				BITMAPINFO bitmapInfo;
//				ZeroMemory(&bitmapInfo.bmiHeader, sizeof(BITMAPINFOHEADER));
//				bitmapInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
//				bitmapInfo.bmiHeader.biWidth  = m_sz.cx;
//				bitmapInfo.bmiHeader.biHeight = -m_sz.cy;
//				bitmapInfo.bmiHeader.biPlanes = 1;
//				bitmapInfo.bmiHeader.biCompression = BI_RGB;
//				bitmapInfo.bmiHeader.biBitCount = 32;
//			 
//				HBITMAP hBitmap = CreateDIBSection(hDc, &bitmapInfo, DIB_RGB_COLORS, (VOID**)&pBitmapBits, NULL, 0);
//				
//				SelectObject(hDc, hBitmap);// 将位图与HDC关联，这样文字时间上就保存在hBitmap里面了
//			 
//				char * p = pBitmapBits + 3;
//				for (int i = 0; i<height; i++)
//					for (int j = 0; j<width; j++, p += 4)
//						*p = 0xFF;
//				RECT rt = { 0,0,width,height };
//				::DrawText(hDc, txt, lstrlen(txt), &rt, DT_SINGLELINE);
//			//	TextOut(hDc,0,0, txt, lstrlen(txt));
//			
//				 //将alpha为0xFF的改为0,为0的改为0xFF
//				p = pBitmapBits + 3;
//				 for (int i = 0; i<height; i++)
//					 for (int j = 0; j<width; j++, p += 4) 
//						 *p = ~(*p);
//
//
//				SAFE_RELEASE(m_pTexture);
//				IDirect3DDevice9* m_pDevice = m_pRenderFactory->GetDevice();
//				HRESULT  hr = m_pDevice->CreateTexture(
//				width,
//				height,
//				0,
//				D3DUSAGE_DYNAMIC,
//				D3DFMT_A8R8G8B8,
//				D3DPOOL_DEFAULT,
//				&m_pTexture,
//				NULL);
//				
//			   if (FAILED(hr)){
//				  m_pTexture = NULL;
//				  goto end;
//			   }
//
//			    ret = 0;
//			    D3DLOCKED_RECT rect;
//			    m_pTexture->LockRect(0, &rect, NULL, D3DLOCK_DISCARD);
//			    unsigned char* dst = (unsigned char*)rect.pBits;   
//				int yxx=0;
//				int row=width*4;
//				char* ImgBuf=(char*)pBitmapBits;
//				for(int i = 0; i < height; ++i) 
//				{
//					memcpy(dst,ImgBuf+yxx ,row);
//					yxx+=row;
//					dst += rect.Pitch;
//				}
//                m_pTexture->UnlockRect(0);
//end:
//				::DeleteObject(hFont);
//				::DeleteObject(hBitmap);
//				::DeleteDC(hDc);
//			//	m_sz.cx *= 0.8;
//				//	m_sz.cy *= 0.8;
//				return  ret;
//
//
//	}
	//int CBitmap_D3D9::LoadText(const char* m_LeftStr,int len,int ff)
	//{
	//	if (len <= 0)
	//		return 0;

	//	static char* ffxx=(char*) GetUTF8String(L"宋体");
	//	static SkTypeface* fn=SkTypeface::CreateFromName(ffxx,SkTypeface::kNormal);

	//				SIZE	m_sizeReset;
	//				HWND hView = GetShellWindow();
	//				HDC dc = GetDC(hView);
	//				GetTextExtentPoint32(dc,( LPCWSTR)m_LeftStr,len,&m_sizeReset);
	//				ReleaseDC(hView,dc);
	//
	//	        SkPaint  paint; 
	//			 paint.setTypeface(fn);
	//			//设置文字编码
	//			paint.setTextEncoding(SkPaint::TextEncoding::kUTF16_TextEncoding);
	//			if(ff==1){
	//				paint.setTextAlign(SkPaint::Align::kRight_Align);
	//			}
	//			else {
 //                   paint.setTextAlign(SkPaint::Align::kLeft_Align);
	//			}
	//			int fontsize=13;
 //               paint.setTextSize(fontsize);
	//		 
	//			SkRect bounds;
	//			SkScalar xxx= paint.measureText(m_LeftStr, len,&bounds);
	//			int width =bounds.right() - bounds.left()+2;
	//			int height = bounds.fBottom - bounds.fTop+2;
	//			//width = width *paint.countText(m_LeftStr, len);
	//			m_sz.cx = width;
	//			m_sz.cy = height;


	//		    SAFE_RELEASE(m_pTexture);
	//		    IDirect3DDevice9* m_pDevice = m_pRenderFactory->GetDevice();
	//		    HRESULT  hr = m_pDevice->CreateTexture(
	//			width,
	//			height,
	//			0,
	//			D3DUSAGE_DYNAMIC,
	//			D3DFMT_A8R8G8B8,
	//			D3DPOOL_DEFAULT,
	//			&m_pTexture,
	//			NULL);
	//			
	//		   if (FAILED(hr)){
	//			  m_pTexture = NULL;
	//			  return false;
	//		   }

	//          //  LPCSTR strFont = GetUTF8String(L"宋体");
	//			//SkTypeface *font = SkTypeface::CreateFromName(strFont, SkTypeface::kNormal);

	//			SkBitmap Skbit;
	//			Skbit.setInfo(SkImageInfo::Make( width,height,SkColorType::kBGRA_8888_SkColorType,SkAlphaType::kPremul_SkAlphaType));
	//			Skbit.allocPixels();
	//			char* ImgBuf= (char*)Skbit.getPixels();
	//			memset(ImgBuf,0,width*height*4);

 //               int buflen=Skbit. getSize();
	//			SkCanvas canvas(Skbit);
	//			

	//			paint.setAntiAlias(true);
	//			paint.setARGB(255, 255,0, 0); 
	//			canvas.drawText(m_LeftStr, len, 0,fontsize, paint);

	//			D3DLOCKED_RECT rect;//1009*488
	//		    m_pTexture->LockRect(0, &rect, NULL, D3DLOCK_DISCARD);


	//		
	//			unsigned char* dst = (unsigned char*)rect.pBits;   
	//			memset(dst,0,rect.Pitch*height);

	//		
	//			int yxx=0;
	//			int row=Skbit.rowBytes();
	//			for(int i = 0; i < height; ++i) 
	//			{
	//				memcpy(dst,ImgBuf+yxx ,row);
	//				yxx+=row;
	//				dst += rect.Pitch;
	//			}
 //               m_pTexture->UnlockRect(0);	
	//		
	//			return 0;
	//}
   CRenderTarget_D3D9::CRenderTarget_D3D9( SRenderFactory_D3D9* pRenderFactory,HWND h,int nWidth, int nHeight): 
	    m_hView( h),
	    m_pRenderFactory(pRenderFactory), 
		m_nWidth(nWidth),
		m_nHeight(nHeight),
		m_pSwapChainSurface9(0),
		m_pSwapChain(0)
   {
	   m_pRenderFactory->AddRef();
       RecreateSwapChain();   
	}
   CRenderTarget_D3D9::~CRenderTarget_D3D9()
   {
	   Release2();
       m_pRenderFactory->Release();
	    m_pRenderFactory->RemoveObj(this);
	  
   }
   void  CRenderTarget_D3D9::Release2()
   {
	   SAFE_RELEASE( m_pSwapChainSurface9);
	   SAFE_RELEASE( m_pSwapChain);
  }
    void  CRenderTarget_D3D9::ReLoadRes()
	{
	      RecreateSwapChain(); 
	}
   void   CRenderTarget_D3D9::ReSize(int nWidth, int nHeight)
   {
         m_nWidth=nWidth;
		 m_nHeight=nHeight;
		 m_pRenderFactory->Lock();
		 RecreateSwapChain(); 
		 m_pRenderFactory->UnLock();
   }
   void CRenderTarget_D3D9::RecreateSwapChain()
   {
	  
	   HRESULT hr= 0;
       if(m_pSwapChainSurface9)
	   {
			hr= m_pSwapChainSurface9->Release();
			//int xxx =m_pSwapChainSurface9->AddRef();
			m_pSwapChainSurface9=0;
		}
		if(m_pSwapChain)
		{
			hr= m_pSwapChain->Release();
			m_pSwapChain=0;
		}
 
         if(m_nWidth <=0 || m_nHeight <=0 )
		   return ;

		D3DPRESENT_PARAMETERS PresentParams = GetPresentParams(m_hView);
		PresentParams.SwapEffect= D3DSWAPEFFECT_COPY;
		PresentParams.hDeviceWindow = m_hView;
		PresentParams.BackBufferHeight = m_nHeight;
		PresentParams.BackBufferWidth = m_nWidth;

		 hr= m_pRenderFactory->GetDevice()->CreateAdditionalSwapChain(&PresentParams,&m_pSwapChain);
        if(FAILED(hr) )
		{
		   assert(0);
		}

		hr =m_pSwapChain->GetBackBuffer(0,D3DBACKBUFFER_TYPE_MONO,&m_pSwapChainSurface9);
		if(FAILED(hr) )
		{
		  assert(0);
		}
		m_pSwapChainSurface9->Release();
   }
  

   int  CRenderTarget_D3D9::DrawBitmap(kkRect* pRcDest,IBitmap *pBitmap,int xSrc,int ySrc,int cx,int cy,unsigned char byAlpha)
   {
	   //FillRect(pRcDest, 0xffff0000);
		 kkSize sz=pBitmap->Size();
	     KKUI_2DVertex vec[4];
		
		 GetUIRHWVertex(vec, *pRcDest,D3DCOLOR_ARGB( 255, 255, 255, 255 ), xSrc,ySrc,cx,cy,sz.cx,sz.cy);
		// GetUIRHWVertex(vec, *pRcDest, D3DCOLOR_ARGB(255, 255, 255, 255), 0, 0, 144, 48, 144, 48);

	     CBitmap_D3D9* bit =( CBitmap_D3D9*) pBitmap;
	     IDirect3DDevice9* pDevice=m_pRenderFactory->GetDevice();
		 IDirect3DTexture9*  tex = bit->GetTexture();
		 if (tex == 0)
			 return 0;
        int hh= pDevice->SetTexture(0, bit->GetTexture());
         pDevice->SetFVF(KKUI_2DVertex::FVF);
		 pDevice->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
		
		  //放大系数 ,放大采样方式
		 pDevice->SetSamplerState(0, D3DSAMP_MAGFILTER,   D3DTEXF_LINEAR);
		 //缩小采样，方式
         pDevice->SetSamplerState(0, D3DSAMP_MINFILTER,   D3DTEXF_LINEAR);
         pDevice->SetSamplerState(0,D3DSAMP_MIPFILTER,   D3DTEXF_LINEAR);
		
		 pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE); 
		 pDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
		 pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		 pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
		 pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2,  vec, sizeof(KKUI_2DVertex));/**/
		 pDevice->SetTexture(0, NULL) ;
		 return 1;
   }
   int  CRenderTarget_D3D9::DrawBitmapEx(kkRect*  pRcDest,IBitmap *pBitmap,kkRect* pRcSrc,unsigned int expendMode,unsigned char byAlpha/**/ )
   {
	    unsigned int expandModeLow = LOWORD(expendMode);
        
		kkSize sz=pBitmap->Size();
        if(expandModeLow == EM_NULL)
            return DrawBitmap(pRcDest,pBitmap,pRcSrc->left,pRcSrc->top,sz.cx,sz.cy,byAlpha);

       
        
        if(expandModeLow == EM_STRETCH)
        {
            //::AlphaBlend(m_hdc,pRcDest->left,pRcDest->top,pRcDest->right-pRcDest->left,pRcDest->bottom-pRcDest->top,
           //hmemdc,pRcSrc->left,pRcSrc->top,pRcSrc->right-pRcSrc->left,pRcSrc->bottom-pRcSrc->top,bf);

			DrawBitmap(pRcDest,pBitmap,pRcSrc->left,pRcSrc->top,pRcSrc->right-pRcSrc->left,pRcSrc->bottom-pRcSrc->top,byAlpha);
        }else
        {
           
           // ::IntersectClipRect(m_hdc,pRcDest->left,pRcDest->top,pRcDest->right,pRcDest->bottom);
            int nWid=pRcSrc->right-pRcSrc->left;
            int nHei=pRcSrc->bottom-pRcSrc->top;
            for(int y=pRcDest->top ;y<pRcDest->bottom;y+=nHei)
            {
                for(int x=pRcDest->left; x<pRcDest->right; x+=nWid)
                {
                   // ::AlphaBlend(m_hdc,x,y,nWid,nHei,
                     //   hmemdc,pRcSrc->left,pRcSrc->top,nWid,nHei,
                    //    bf);                    
                }
            }
     
        }
       
        return S_OK;
       return 1;
   }
   int  CRenderTarget_D3D9::DrawBitmap9Patch(kkRect* pRcDest,IBitmap *pBitmap,kkRect* pRcSrc,kkRect* pRcSourMargin,unsigned expendMode,unsigned char byAlpha)
   {
      
        int xDest[4] = {pRcDest->left,pRcDest->left+pRcSourMargin->left,pRcDest->right-pRcSourMargin->right,pRcDest->right};
        int xSrc[4] = {pRcSrc->left,pRcSrc->left+pRcSourMargin->left,pRcSrc->right-pRcSourMargin->right,pRcSrc->right};
        int yDest[4] = {pRcDest->top,pRcDest->top+pRcSourMargin->top,pRcDest->bottom-pRcSourMargin->bottom,pRcDest->bottom};
        int ySrc[4] = {pRcSrc->top,pRcSrc->top+pRcSourMargin->top,pRcSrc->bottom-pRcSourMargin->bottom,pRcSrc->bottom};

        //首先保证九宫分割正常
        if(!(xSrc[0] <= xSrc[1] && xSrc[1] <= xSrc[2] && xSrc[2] <= xSrc[3])) 
			return S_FALSE;
        if(!(ySrc[0] <= ySrc[1] && ySrc[1] <= ySrc[2] && ySrc[2] <= ySrc[3])) 
			return S_FALSE;

        //调整目标位置
        int nDestWid=pRcDest->right-pRcDest->left;
        int nDestHei=pRcDest->bottom-pRcDest->top;

        if((pRcSourMargin->left + pRcSourMargin->right) > nDestWid)
        {//边缘宽度大于目标宽度的处理
            if(pRcSourMargin->left >= nDestWid)
            {//只绘制左边部分
                xSrc[1] = xSrc[2] = xSrc[3] = xSrc[0]+nDestWid;
                xDest[1] = xDest[2] = xDest[3] = xDest[0]+nDestWid;
            }else if(pRcSourMargin->right >= nDestWid)
            {//只绘制右边部分
                xSrc[0] = xSrc[1] = xSrc[2] = xSrc[3]-nDestWid;
                xDest[0] = xDest[1] = xDest[2] = xDest[3]-nDestWid;
            }else
            {//先绘制左边部分，剩余的用右边填充
                int nRemain=xDest[3]-xDest[1];
                xSrc[2] = xSrc[3]-nRemain;
                xDest[2] = xDest[3]-nRemain;
            }
        }

        if(pRcSourMargin->top + pRcSourMargin->bottom > nDestHei)
        {
            if(pRcSourMargin->top >= nDestHei)
            {//只绘制上边部分
                ySrc[1] = ySrc[2] = ySrc[3] = ySrc[0]+nDestHei;
                yDest[1] = yDest[2] = yDest[3] = yDest[0]+nDestHei;
            }else if(pRcSourMargin->bottom >= nDestHei)
            {//只绘制下边部分
                ySrc[0] = ySrc[1] = ySrc[2] = ySrc[3]-nDestHei;
                yDest[0] = yDest[1] = yDest[2] = yDest[3]-nDestHei;
            }else
            {//先绘制左边部分，剩余的用右边填充
                int nRemain=yDest[3]-yDest[1];
                ySrc[2] = ySrc[3]-nRemain;
                yDest[2] = yDest[3]-nRemain;
            }
        }

        //定义绘制模式
        unsigned int mode[3][3]={
            {EM_NULL,expendMode,EM_NULL},
            {expendMode,expendMode,expendMode},
            {EM_NULL,expendMode,EM_NULL}
        };

        for(int y=0;y<3;y++)
        {
            if(ySrc[y] == ySrc[y+1]) 
				continue;
            for(int x=0;x<3;x++)
            {
                if(xSrc[x] == xSrc[x+1])
					continue;
                kkRect rcSrc = {xSrc[x],ySrc[y],xSrc[x+1],ySrc[y+1]};
                kkRect rcDest ={xDest[x],yDest[y],xDest[x+1],yDest[y+1]};
                DrawBitmapEx(&rcDest,pBitmap,&rcSrc,mode[y][x],byAlpha);
            }
        }

        return S_OK;
   }
    //http://www.bubuko.com/infodetail-1229033.html
    //https://blog.csdn.net/fox64194167/article/details/8540600
    //2d渲染 https://www.cnblogs.com/kex1n/archive/2013/10/23/3384890.html

 //  if(0){
	//D3DXMATRIX Ortho2D;
 //   D3DXMATRIX Identity;

	////创建一个左手坐标系正交投影矩阵
 //   D3DXMatrixOrthoLH(&Ortho2D, 100, 100, 0.0f, 1.0f);
 //   D3DXMatrixIdentity(&Identity);

 //   g_pd3dDevice->SetTransform(D3DTS_PROJECTION, &Ortho2D);
 //   g_pd3dDevice->SetTransform(D3DTS_WORLD, &Identity);
 //   g_pd3dDevice->SetTransform(D3DTS_VIEW, &Identity);/**/
	//}
    int   CRenderTarget_D3D9::DrawYuv420p(kkRect* pRcDest,IPicYuv420p *pYuv420p,int xSrc,int ySrc,unsigned char byAlpha,unsigned char R,unsigned char G,unsigned char B)
	{
		 CYuv420p_D3D9* pYUV=(CYuv420p_D3D9*)pYuv420p; 
		 IDirect3DDevice9*   pDevice=m_pRenderFactory->GetDevice();
		 IDirect3DTexture9*  pTexture = pYUV->GetTexture();
		// ID3DXSprite*       pSprite=m_pRenderFactory->GetSprite(){return m_pSprite;}
		 if(pTexture){
			 KKUI_Vertex vec[4];
			 GetUIVertex(vec, *pRcDest,xSrc,ySrc,D3DCOLOR_ARGB( byAlpha, R, G, B ),pYuv420p->Size().cx,pYuv420p->Size().cy,m_nWidth,m_nHeight);

			 D3DXMATRIX RotationY;
			 D3DXMatrixIdentity(&RotationY);
            // fpDX9_D3DXMatrixRotationZ (&RotationY,90);
 
			int angle = 90;
          /*  RotationY._11 = cosf(angle) ;
            RotationY._12 = sinf(angle) ;
            RotationY._21 =-sinf(angle) ;
            RotationY._22 = cosf(angle) ;*/
			//RotationY._42 = 1.0f;
        /*    RotationY._42 = 1.0f;
            RotationY._43 = 0.0f;*/

			// fpDX9_D3DXMatrixRotationY(&RotationY,30);
			// pDevice->GetTransform(D3DTS_TEXTURE0,&pOldMarix);
			
			 //pDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID) ; 
			 pDevice->SetTexture(0, pTexture);
			 pDevice->SetFVF(KKUI_Vertex::FVF);

			//pSprite->

			 //禁用光照
             pDevice->SetRenderState(D3DRS_LIGHTING,0);
			 //放大系数 ,放大采样方式
			 pDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
			 //缩小采样，方式
             pDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
			 //当物体越来越远离观察者，在屏幕上被着色的像素随着观察者的远离也越来越少
             pDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);/**/

			 //D3DTADDRESS_WRAP    重叠纹理寻址
		     //D3DTADDRESS_MIRROR  镜像纹理寻址模式
			pDevice->SetSamplerState( 0, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP );  //重叠纹理寻址
			pDevice->SetSamplerState( 0, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP );  //重叠纹理寻址



			
			
			 
			

			 /* pDevice->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
			 
			
			 pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);*/
			// pDevice->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
		    // pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);

			 //透明度计算 公式color = source_color * source_alpha + dest_color * ( 1 - source_alpha );

			 //透明度计算
			 //if(byAlpha!=0xFF)
			 {
				 ////开启alpha test  
				 pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE); 

				 pDevice->SetRenderState( D3DRS_BLENDOP, D3DBLENDOP_ADD );
				 //// alpha 值来自于源图像 
				 pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
				 // 1 - source_alpha
				 pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA );


				 pDevice->SetTextureStageState( 0, D3DTSS_COLOROP, D3DTOP_MODULATE );
				 pDevice->SetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE );
				 pDevice->SetTextureStageState( 0, D3DTSS_COLORARG2, D3DTA_CURRENT );

				 pDevice->SetTextureStageState( 0, D3DTSS_ALPHAOP, D3DTOP_MODULATE );
				 pDevice->SetTextureStageState( 0, D3DTSS_ALPHAARG1, D3DTA_DIFFUSE );
				 pDevice->SetTextureStageState( 0, D3DTSS_ALPHAARG2, D3DTA_TEXTURE );
			 }


			// pDevice->SetTextureStageState(0, D3DTSS_TEXCOORDINDEX,            D3DTSS_TCI_CAMERASPACEPOSITION);   
			// int hr=pDevice->SetTextureStageState(0,D3DTSS_TEXTURETRANSFORMFLAGS,D3DTTFF_COUNT2);
			// pDevice->SetTransform(D3DTS_TEXTURE0, &RotationY);

			/* D3DXMATRIX mat_texture, mat_scale, mat_trans;

             D3DXMatrixIdentity(&mat_texture);
             fpDX9_D3DXMatrixScaling(&mat_scale, 0.5f, -0.5f, 1.0f);
             fpDX9_D3DXMatrixTranslation(&mat_trans, 0.5f, 0.5f, 1.0f);

             mat_texture = mat_texture * mat_scale * mat_trans;*/
         //    hr=pDevice->SetTransform(D3DTS_TEXTURE0, &mat_texture);

			 ////控制制定A的输出
             //pDevice->SetRenderState( D3DRS_ALPHATESTENABLE, TRUE );
			 //pDevice->SetRenderState( D3DRS_ALPHAFUNC, D3DCMP_GREATER );
			 //pDevice->SetRenderState( D3DRS_ALPHAREF, 0 );


			 pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2,  vec, sizeof(KKUI_Vertex));/**/
            
			  
            //  D3DXCreateSprite

			 
			 


			// pDevice->SetTransform(D3DTS_TEXTURE0,&pOldMarix);
			 pDevice->SetTexture(0, NULL) ;

		 }else{
				 IDirect3DSurface9* pSur=pYUV->GetSurface();
				 if(!pSur)
				 {
					return 0;
				 }
				
				 pDevice->StretchRect(pSur,NULL,m_pSwapChainSurface9,(RECT*)pRcDest,D3DTEXF_LINEAR); 
		 }
		 return 1;
	}
    struct CUSTOMVERTEX
	{
		float x, y, z, rhw; // The transformed position for the vertex
		DWORD color;        // The vertex color
	};

	#define D3DFVF_CUSTOMVERTEX (D3DFVF_XYZRHW|D3DFVF_DIFFUSE)
   void   CRenderTarget_D3D9::DrawLine(int startx,int starty, int endx,int endy, unsigned int color)
   {
        IDirect3DDevice9* pDevice=m_pRenderFactory->GetDevice();
		LPDIRECT3DVERTEXBUFFER9  pVB = NULL;
        CUSTOMVERTEX vertices[] =
		{
			{ startx,  starty, 0.5f, 1.0f, 0xffff0000, }, // x, y, z, rhw, color
			{ endx,    endy, 0.5f, 1.0f, 0xffff0000, },
			
		};

		// Create the vertex buffer. Here we are allocating enough memory
		// (from the default pool) to hold all our 3 custom vertices. We also
		// specify the FVF, so the vertex buffer knows what data it contains.
		if( pDevice->CreateVertexBuffer( 3 * sizeof( CUSTOMVERTEX ),
													  0, D3DFVF_XYZRHW,
													  D3DPOOL_DEFAULT, &pVB, NULL))
		{
			return ;
		}

		// Now we fill the vertex buffer. To do this, we need to Lock() the VB to
		// gain access to the vertices. This mechanism is required becuase vertex
		// buffers may be in device memory.
		VOID* pVertices;
		if( FAILED( pVB->Lock( 0, sizeof( vertices ), ( void** )&pVertices, 0 ) ) )
			return ;
		memcpy( pVertices, vertices, sizeof( vertices ) );
		pVB->Unlock();

		pDevice->SetStreamSource( 0, pVB, 0, sizeof( CUSTOMVERTEX ) );
        pDevice->SetFVF(D3DFVF_CUSTOMVERTEX );

		pDevice->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
		pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);

        pDevice->DrawPrimitive( D3DPT_LINELIST, 0, 1 );
		int xx=pVB->AddRef();
		pVB->Release();
		pVB->Release();
   }



   void   CRenderTarget_D3D9::DrawText(IFont* pFont,wchar_t *text,kkRect* pRcDest)
   {
		   CFont_D3D9* ff =(CFont_D3D9*)pFont;
		   ID3DXFont* Font= ff->GetD3DXFont();
				 
			RECT rt={pRcDest->left,pRcDest->top,pRcDest->right,pRcDest->bottom };
			Font->DrawTextW(0,text,lstrlen(text),&rt,DT_LEFT, 0xFFFFFFFF);
   }
  
   void  CRenderTarget_D3D9::MeasureText(IFont* pFont,wchar_t *text,kkSize *pSz )
   {
	   CFont_D3D9* ff =(CFont_D3D9*)pFont;
	   ID3DXFont* Font= ff->GetD3DXFont();

	   HDC hdc=Font->GetDC();

	   SIZE sz;
	   GetTextExtentPoint32(hdc, text, lstrlen(text), &sz);
	   pSz->cx = sz.cx;
	   pSz->cy = sz.cy;
				   // IDirect3DDevice9* pDevice=m_pRenderFactory->GetDevice();
   }
   int   CRenderTarget_D3D9::FillRect(kkRect* rt,unsigned int cr)
   {
	    //cr=0xffff0000;
	    IDirect3DDevice9* pDevice=m_pRenderFactory->GetDevice();
		pDevice->SetTexture(0, NULL) ;
		LPDIRECT3DVERTEXBUFFER9  pVB = NULL;
        CUSTOMVERTEX vertices[] =
		{
			{ rt->left,   rt->top,   0, 1.0f, cr, }, // x, y, z, rhw, color
		
			{ rt->right,  rt->top,    0, 1.0f, cr, },
	        { rt->right,  rt->bottom, 0, 1.0f, cr, },

			{ rt->right,  rt->bottom,   0, 1.0f, cr, }, // x, y, z, rhw, color
			
			{ rt->left,  rt->bottom,  0, 1.0f, cr, },
			{ rt->left,  rt->top,  0, 1.0f, cr, },
		};

		// Create the vertex buffer. Here we are allocating enough memory
		// (from the default pool) to hold all our 3 custom vertices. We also
		// specify the FVF, so the vertex buffer knows what data it contains.
		if( pDevice->CreateVertexBuffer(6 * sizeof( CUSTOMVERTEX ),
													  0, D3DFVF_XYZRHW,
													  D3DPOOL_DEFAULT, &pVB, NULL))
		{
			return 0;
		}

		// Now we fill the vertex buffer. To do this, we need to Lock() the VB to
		// gain access to the vertices. This mechanism is required becuase vertex
		// buffers may be in device memory.
		VOID*   pVertices;
		HRESULT hr = pVB->Lock(0, sizeof(vertices), (void**)&pVertices, 0);
		if(hr)
			return 0;
		memcpy( pVertices, vertices, sizeof( vertices ) );
		pVB->Unlock();

		pDevice->SetStreamSource( 0, pVB, 0, sizeof( CUSTOMVERTEX ) );
        pDevice->SetFVF(D3DFVF_CUSTOMVERTEX );

		pDevice->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
		pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);

        pDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0,2 );
		int xx=pVB->AddRef();
		pVB->Release();
		pVB->Release();
		return 1;
	}
}


extern "C"
{
	void  __declspec(dllexport) CreateRenderFactoryD3D9(KKUI::IRenderFactory** ff)
	{
		KKUI::SRenderFactory_D3D9* f = new KKUI::SRenderFactory_D3D9();
		*ff=f;
	}
}