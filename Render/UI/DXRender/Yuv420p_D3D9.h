#ifndef Yuv420p_D3D9_H_
#define Yuv420p_D3D9_H_
#include <map>
#include <string>
#include <d3d9.h>
#include "ObjRef.h"
#include "Core/ObjectImp.h"
#include "Core/IRender.h"
namespace KKUI
{
	class SRenderFactory_D3D9;
	class CYuv420p_D3D9:public TObjRefImpl<IPicYuv420p>
	{
	        public:
				    CYuv420p_D3D9(	SRenderFactory_D3D9* pRenderFactory);
					~CYuv420p_D3D9();
                    void                Release2();
					void                ReLoadRes();
					kkSize              Size() const{return  m_Size;};
					
                    int	                Load(const kkPicInfo* data);
					IDirect3DSurface9*  GetSurface(){   return m_pYUV420pSurface;}
					IDirect3DTexture9*  GetTexture(){   return m_pRenderTexture; }
	        private:
				   int	      LoadData(const kkPicInfo* data);
				    SRenderFactory_D3D9*    m_pRenderFactory;
					//yuv 
				    IDirect3DSurface9*      m_pYUV420pSurface;

					IDirect3DTexture9*      m_pRenderTexture;
					kkSize    m_Size;
	};
}
#endif