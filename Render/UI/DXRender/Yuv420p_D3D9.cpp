#include "RenderTarget_D3D9.h"
#include "Yuv420p_D3D9.h"
namespace KKUI
{
	CYuv420p_D3D9::CYuv420p_D3D9(SRenderFactory_D3D9* pRenderFactory):m_pYUV420pSurface(0),m_pRenderFactory(pRenderFactory),m_pRenderTexture(0)
	{
		m_pRenderFactory->AddRef();
		m_Size.cx = 0;
		m_Size.cy = 0;
	}
	
	CYuv420p_D3D9::~CYuv420p_D3D9()
	{
		m_pRenderFactory->Release();
	    Release2();
		m_pRenderFactory->RemoveObj(this);
	}
    void CYuv420p_D3D9:: Release2()
	{
		 if(m_pYUV420pSurface)
			   m_pYUV420pSurface->Release();
		   m_pYUV420pSurface = 0;

		   if(m_pRenderTexture)
			   m_pRenderTexture->Release();
			m_pRenderTexture = 0;
	}
	void  CYuv420p_D3D9::ReLoadRes()
	{
	
	}
	int	 CYuv420p_D3D9::LoadData(const kkPicInfo* Picinfo)
	{

		IDirect3DDevice9* pDevice= m_pRenderFactory-> GetDevice();
		m_pRenderFactory->Lock();
		if(m_pRenderFactory->LostDevice())
		{
			m_pRenderFactory->UnLock();
		     return 0;
		}

		if (m_pYUV420pSurface == NULL||m_Size.cx!=Picinfo->width|| m_Size.cy!=Picinfo->height ){
				if(m_pYUV420pSurface)
				   m_pYUV420pSurface->Release();

			
				D3DFORMAT d3dformat=(D3DFORMAT)MAKEFOURCC('Y', 'V', '1', '2');
				/*if(Picinfo->picformat==(int)AV_PIX_FMT_NV12)
					d3dformat=(D3DFORMAT)MAKEFOURCC('N', 'V', '1', '2');*/

				//DX YV12 ����YUV420P
				HRESULT hr= pDevice->CreateOffscreenPlainSurface(
					Picinfo->width, Picinfo->height,
					d3dformat,
					D3DPOOL_DEFAULT,
					//D3DPOOL_SYSTEMMEM,
					&m_pYUV420pSurface,
					NULL);

				if (FAILED(hr)){
					    m_pRenderFactory->UnLock();
						return 0; 
				}
			}

			  m_Size.cx = Picinfo->width;
			  m_Size.cy = Picinfo->height;
			  D3DLOCKED_RECT rect;
			  m_pYUV420pSurface->LockRect(&rect,NULL,D3DLOCK_DONOTWAIT);  
			  if(rect.pBits!=NULL)
			  {			  
					  byte *pSrc = (byte *)Picinfo->data[0];
					  byte * pDest = (BYTE *)rect.pBits;  
					  int stride = rect.Pitch;   
					  int pixel_h=Picinfo->height;
					  int pixel_w=Picinfo->width; 
					  int i = 0;  
					 
					     
						 
						  byte *pY=Picinfo->data[0];
						  byte *pU=Picinfo->data[1];
						  byte *pV=Picinfo->data[2];
						  //Copy Data (YUV420P)  
						  //Y
						  for(i = 0;i < pixel_h;i ++)
						  {  
							  memcpy(pDest + i * stride,pY + i * Picinfo->linesize[0],Picinfo->linesize[0]);  
						  } 
						  //U
						  for(i = 0;i < pixel_h/2;i ++)
						  {  
							  memcpy(pDest + stride * pixel_h + i * stride / 2,pV + i * Picinfo->linesize[2], Picinfo->linesize[2]);  
						  }  
						  //V
						  for(i = 0;i < pixel_h/2;i ++)
						  {  
							  memcpy(pDest + stride * pixel_h + stride * pixel_h / 4 + i * stride / 2,pU + i * Picinfo->linesize[2], Picinfo->linesize[2]);  
						  } /**/
					
			}
			m_pYUV420pSurface->UnlockRect();
			m_pRenderFactory->UnLock();
			return 1;
	}
	int CYuv420p_D3D9::Load(const kkPicInfo* Picinfo)
	{
		m_pRenderFactory->Lock();
		   IDirect3DDevice9* pDevice= m_pRenderFactory-> GetDevice();
	       LoadData(Picinfo);   
            HRESULT hr =0;
		   if(!m_pRenderTexture){
		        hr =  pDevice->CreateTexture(
			    Picinfo->width, Picinfo->height,
				1,
				D3DUSAGE_RENDERTARGET,
				D3DFMT_A8R8G8B8,//D3DFMT_R5G6B5,
				D3DPOOL_DEFAULT,
				&m_pRenderTexture,
				NULL) ;
				if (FAILED(hr))
				{
					 assert(0);
				}
		   }
		    
		    //��Ⱦ������
			IDirect3DSurface9*        pRenderSurface    = NULL ;
			hr = m_pRenderTexture->GetSurfaceLevel(0, &pRenderSurface) ;
			if (FAILED(hr))
			{
				 assert(0);
			}

			IDirect3DSurface9*        pOldRenderSurface    = NULL ;
			pDevice->GetRenderTarget(0,&pOldRenderSurface);

			pDevice->SetRenderTarget(0, pRenderSurface);
            pDevice->Clear( 0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0xff000000, 1.0f, 0 );
 
			if( SUCCEEDED( pDevice->BeginScene() ) )
			{
				pDevice->SetTexture(0, NULL) ;
				RECT rt;
				rt.left =0 ;
				rt.top =0;
				rt.bottom = Picinfo->height;
				rt.right = Picinfo->width;
				hr = pDevice->StretchRect( m_pYUV420pSurface,NULL, pRenderSurface,&rt,D3DTEXF_LINEAR);

				pDevice->EndScene();
			}
			pRenderSurface->Release();
			if(pOldRenderSurface){
				pDevice->SetRenderTarget(0, pOldRenderSurface) ;
				pOldRenderSurface->Release();
			}
		   m_pRenderFactory->UnLock();
		return 1;
	}
}