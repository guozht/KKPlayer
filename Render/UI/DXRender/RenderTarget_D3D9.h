#ifndef RenderTarget_D3D9_H_
#define RenderTarget_D3D9_H_

#include <map>
#include <set>
#include <string>
#include <d3d9.h>
#include <d3dx9core.h>
#include "ObjRef.h"
#include "Core/ObjectImp.h"
#include "Core/IRender.h"
#include "RendLock.h"

#include <string>
namespace KKUI
{

	class SRenderFactory_D3D9:public  TObjRefImpl<IRenderFactory>
    {
		public:
				SRenderFactory_D3D9();
				~SRenderFactory_D3D9();
				bool         init();
				int          CreateRenderTarget(IRenderTarget ** ppRenderTarget,void* Handle,int nWid,int nHei);
				int          CreateBitmap(IBitmap ** ppBitmap);
				int          CreateYUV420p(IPicYuv420p ** ppBitmap);
                void         BeforePaint(IRenderTarget* pRenderTarget);
				void         EndPaint(IRenderTarget* pRenderTarget);
			    void         CreateFont(IFont** ppFont,int FontSize,wchar_t* FontName);
                void         Lock();
				void         UnLock();
				IDirect3DDevice9*  GetDevice(){return m_pDevice;}
				bool         LostDevice();
				bool         LostDeviceRestore();

				//移除创建的对象
				void         RemoveObj(IRenderObj* obj);
	    private:
			     IDirect3D9*            m_pD3D;
                 IDirect3DDevice9*      m_pDevice;
				// ID3DXSprite*       m_pSprite;
				 CRendLock              m_lock;
				 //呈现对象管理器
				 std::set<IRenderObj*>  m_IRenderObjSet;
				 
    };

	class CFont_D3D9:public TObjRefImpl<IFont>
	{
		public:
			CFont_D3D9(SRenderFactory_D3D9* pRenderFactory,int FontSize,wchar_t* FontName);
			~CFont_D3D9();
			void          Release2();
			void          ReLoadRes();
			int           TextSize();
			ID3DXFont*    GetD3DXFont();
		private:
			ID3DXFont*              m_pD3DXFont;
			SRenderFactory_D3D9*    m_pRenderFactory;
			int                     m_nTextSize;
			wchar_t                 m_nFontName[64];
	};

	class CBitmap_D3D9:public TObjRefImpl<IBitmap>
	{
		public:
			CBitmap_D3D9(SRenderFactory_D3D9* pRenderFactory);
			~CBitmap_D3D9();
			void          Release2();
			void          ReLoadRes();
			virtual kkSize    Size() const;
			virtual int       Width();
			virtual int       Height();
			int               LoadFromFile(const char*FileName);
            int	              LoadFromMemory(const void* pBuf,int szLen);
		
			IDirect3DTexture9*  GetTexture(){ return m_pTexture;}
		private:
			IDirect3DTexture9*      m_pTexture;
			SRenderFactory_D3D9*    m_pRenderFactory;
			//暂且先缓存
			void*                   m_pSrcBuffer;
			int                     m_pSrcBufferLen;
			kkSize                  m_sz;
	};
	class CRenderTarget_D3D9: public TObjRefImpl<IRenderTarget>
	{
		  public:
			     CRenderTarget_D3D9(SRenderFactory_D3D9* pRenderFactory,HWND h,int nWidth, int nHeight);
			     ~CRenderTarget_D3D9();
				 void  Release2();
				 void  ReLoadRes();
				 void  ReSize(int nWidth, int nHeight);
				 //绘制位图
				 int   DrawBitmap(kkRect* pRcDest,IBitmap *pBitmap,int xSrc,int ySrc,int cx,int cy,unsigned char byAlpha=0xFF);
				 int   DrawBitmapEx(kkRect*  pRcDest,IBitmap *pBitmap,kkRect* pRcSrc,unsigned int expendMode,unsigned char byAlpha=0xFF/**/ );
				 int   DrawBitmap9Patch(kkRect* pRcDest,IBitmap *pBitmap,kkRect* pRcSrc,kkRect* pRcSourMargin,unsigned expendMode,unsigned char byAlpha=0xFF);
				 int   DrawYuv420p(kkRect* pRcDest,IPicYuv420p *pYuv420p,int xSrc,int ySrc,unsigned char byAlpha=0xff,unsigned char R=0xFF,unsigned char G=0xFF,unsigned char B=0xFF);

                 void  DrawLine(int startx,int starty, int endx,int endy, unsigned int color);
                 void  DrawText(IFont* pFont,wchar_t *text,kkRect* pRcDest);
				 void  MeasureText(IFont* pFont,wchar_t *text,kkSize *pSz );

				 int   FillRect(kkRect* rt,unsigned int cr);
				 int   GetWidth(){return    m_nWidth;}
				 int   GetHeight(){return   m_nHeight;}
	     public:
				 IDirect3DSurface9*     GetSurface(){return m_pSwapChainSurface9;}
				 IDirect3DSwapChain9*   GetSwapChain(){return  m_pSwapChain;}
	     private:
			     void RecreateSwapChain();
			     int m_nWidth;
				 int m_nHeight;
		         HWND m_hView;
				 SRenderFactory_D3D9*     m_pRenderFactory;
				 IDirect3DSwapChain9*     m_pSwapChain;
	             IDirect3DSurface9 *      m_pSwapChainSurface9;

	};
}

#endif