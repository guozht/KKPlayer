#include "Core/IUI.h"
#include "Core/UIWindow.h"
#include "Core/UIApp.h"
#include "Core/UIContainerImpl.h"
#include "Core/UIMoveState.h"
#include "Core/UIYUV.h"
//#include "GnText/GnTextMgr.h"
KKUI::CUIApp*  G_pUIApp=0;



extern "C"
{

	void __declspec(dllexport) UI_Ini(KKUI::IRenderFactory* rf)
	{
		if(G_pUIApp==0){
			G_pUIApp= new KKUI::CUIApp();
			G_pUIApp->SetRenderFactory(rf);
			//CGnTextMgr::GetIns();
		}
	}

	///创建一个UI控件
	kkhandle __declspec(dllexport) UI_CreateCtl(char* uiclassname)
	{
		 return KKUI::CUIApp::getSingleton().CreateUIByName(uiclassname);
	}

	//获取控件属性
    int      __declspec(dllexport)   UI_GetCtlAttr(KKUIHandle handle,char* AttrName ,char* OutAttr,int Attrlen)
    {
	      if(OutAttr == 0 || Attrlen == 0 || handle == 0 )
			  return 0;
		   KKUI::CUIWindow*  ui = (KKUI::CUIWindow*)(handle);
		   KKUI::SStringA strOutAttr;
		   int ret=ui->GetCtlAttr(AttrName,strOutAttr);
		   if(ret){
		      strcpy(OutAttr,strOutAttr.GetBuffer(64));
		   }
		   return ret;
	}

	void __declspec(dllexport) UI_SetUIMessage (KKUIContainer handle ,KKUI::IUIMessage* call)
	{
	      KKUI::CUIContainerImpl* Container = (KKUI::CUIContainerImpl*)(handle);

		  Container->SetMessage(call);
	}
	
    void  __declspec(dllexport)  UI_SetIRenderFactory(KKUIContainer handle,KKUI::IRenderFactory* pRenderFactory)
	{
			  KKUI::CUIContainerImpl* ui= (KKUI::CUIContainerImpl*)(handle);
			  if(ui){
				  ui-> SetIRenderFactory(pRenderFactory);
			  }	  
	}

   void  __declspec(dllexport)   UI_Opain(KKUIContainer handle,KKUI::IRenderTarget* Render)
   {
	  KKUI::CUIContainerImpl* ui= (KKUI::CUIContainerImpl*)(handle);
	  if(ui){
		  ui->OnPrint(Render);
	  }
    }
   void   __declspec(dllexport)   UI_StageOpain(KKUIHandle handle,KKUI::IRenderTarget* ren)
   {
           KKUI:: CUIMoveState* ui= (KKUI::CUIMoveState*)(handle);
           if(ui){
		     ui->OnStatgePaint(ren);
		   }
   }
   void            __declspec(dllexport)   UI_Relayout                (KKUIContainer handle,int x,int y,int cx,int cy)
   {
	    if (cx==0 || cy==0)
            return;

       KKUI::CUIContainerImpl* ui= (  KKUI::CUIContainerImpl*)(handle);
	   KKUI::CRect rt;
	   rt.left =x;
	   rt.top=y;
	   rt.bottom=y+cy;
	   rt.right+=x+cx;
	   ui->OnRelayout(rt);
   }
   int              __declspec(dllexport)  UI_FrameEvent              (KKUIContainer handle,unsigned int uMsg,unsigned int wParam,long lParam)
   {
       KKUI::CUIContainerImpl* ui= (KKUI::CUIContainerImpl*)(handle);
	   return ui->DoFrameEvent(uMsg,wParam,lParam);
   }

   KKUIHandle       __declspec(dllexport) UI_FindCtlName             (KKUIHandle handle,const char* strValue, int bLoading)
   {
	     KKUI::CUIWindow*  ui = (KKUI::CUIWindow*)(handle);
		 return ui->FindChildByName(strValue,bLoading);
   }

   //获取控件名称
   int      __declspec(dllexport)  UI_GetCtlName                    (KKUIHandle handle,char* OutName, int NameLen)
   {
          KKUI::CUIWindow*  ui = (KKUI::CUIWindow*)(handle);  
		  int len = ui->GetName().GetLength();
          if( len> NameLen)
			  return -1;
		  strcpy(OutName,ui->GetName().GetBuffer(10));
		  return 1;
   }

   //获取控件window大小
   void      __declspec(dllexport)  UI_GetCtlWindowRect              (KKUIHandle handle,kkRect* OutRect)
   {
		   if(!OutRect)
			   return ;

           KKUI::CUIWindow*  ui = (KKUI::CUIWindow*)(handle);  
		   ui->GetWindowRect(OutRect);
   }

   //设置控件属性
   void      __declspec(dllexport)  UI_SetCtlAttr                    (KKUIHandle handle,const char* attr,const char* attrvalue)
   {
            KKUI::CUIWindow*  ui = (KKUI::CUIWindow*)(handle);  
		    ui->SetAttribute(attr,attrvalue);
   }

   //获取用户数据
   void       __declspec(dllexport) *UI_GetCtlUserData     (KKUIHandle handle)
   {
         KKUI::CUIWindow*  ui = (KKUI::CUIWindow*)(handle);  
		 return ui->GetUserData();
   }
   //设置用户数据
   void        __declspec(dllexport) UI_SetCtlUserData     (KKUIHandle handle,void* UserData)
   {
          KKUI::CUIWindow*  ui = (KKUI::CUIWindow*)(handle);  
		  ui->SetUserData(UserData);
   }


   void              __declspec(dllexport) UI_OnDestroy                (KKUIContainer handle)
   {
            KKUI::CUIContainerImpl* ui= (KKUI::CUIContainerImpl*)(handle);
			ui->SendMessage(UI_DESTROY);
			delete ui;
   }

   KKUIHandle        __declspec(dllexport) UI_CreateChildren          (KKUIHandle handle,char* strxml)
   {
				   KKUI::CUIWindow* ui= (KKUI::CUIWindow*)(handle);
				   KKUIHandle h= ui->CreateChildren(strxml);
				   ui->UpdateLayout();
				   return h;
   }
   int                __declspec(dllexport) UI_DeleteCtl              (KKUIHandle RootHandle,KKUIHandle Handle)
   {
                    KKUI::CUIWindow*  ui = (KKUI::CUIWindow*)(RootHandle);  
					KKUI::CUIWindow*  ll = (KKUI::CUIWindow*)(Handle);  

					if(!ui->DestroyChild(ll))
						return 0;
					ui->Invalidate();
					return 1;
   }
   int                __declspec(dllexport) UI_YUVLoad                (KKUIYUVHandle handle,kkPicInfo *info)
   { 
                      KKUI::CUIYUV*  ui = (KKUI::CUIYUV*)(handle);
					  ui-> LoadYUV(info);
					  return 0;
   }

   int                __declspec(dllexport) UI_AddRef                 (KKUIHandle handle)
   {
					   KKUI::CUIWindow*  ui = (KKUI::CUIWindow*)(handle);
					   return ui->AddRef();
   }

   int                __declspec(dllexport) UI_Release                (KKUIHandle handle)
   {
					   KKUI::CUIWindow*  ui = (KKUI::CUIWindow*)(handle);
					   return ui->Release();
   }
   int               __declspec(dllexport) UI_SetGetResCallInfo(KKUI_GetRawBufferSize fp1,KKUI_GetRawBuffer fp2)
   {
	                 G_pUIApp->SetGetResCallInfo(fp1,fp2);
                     return 0;
   }

   int         __declspec(dllexport) UI_InitDefSkin        ()
   {
                  return G_pUIApp->IniDefSkin();
   }
}