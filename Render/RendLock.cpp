/**************************kkplayer*********************************************/
/*******************Copyright (c) Saint ******************/
/******************Author: Saint *********************/
/*******************Author e-mail: lssaint@163.com ******************/
/*******************Author qq: 784200679 ******************/
/*******************KKPlayer  WWW: http://www.70ic.com/KKplayer ********************************/
/*************************date��2015-6-25**********************************************/
#include "stdafx.h"
#include "RendLock.h"
/***���ֵ***/
 CRendLock::CRendLock()
 {
	 m_crisec = new  CRITICAL_SECTION ();
	 ::InitializeCriticalSectionAndSpinCount((CRITICAL_SECTION*)m_crisec,4096);
 }
CRendLock::CRendLock(unsigned long  dwSpinCount)
{
   ::InitializeCriticalSectionAndSpinCount((CRITICAL_SECTION*)m_crisec, dwSpinCount);
}
CRendLock::~CRendLock()
{
   ::DeleteCriticalSection((CRITICAL_SECTION*)m_crisec); 
   delete  m_crisec;
}
void CRendLock::Lock()
{
	::EnterCriticalSection((CRITICAL_SECTION*)m_crisec);
}
void CRendLock::Unlock()
{
   ::LeaveCriticalSection((CRITICAL_SECTION*)m_crisec);
}
int CRendLock::TryLock()
{
	return ::TryEnterCriticalSection((CRITICAL_SECTION*)m_crisec);
}