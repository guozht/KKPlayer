/**************************kkplayer*********************************************/
/*******************Copyright (c) Saint ******************/
/******************Author: Saint *********************/
/*******************Author e-mail: lssaint@163.com ******************/
/*******************Author qq: 784200679 ******************/
/*******************KKPlayer  WWW: http://www.70ic.com/KKplayer ********************************/
/*************************date��2015-6-25**********************************************/
#ifndef RendLock_H
#define RendLock_H
/***���ֵ***/
class CRendLock
{
public:
	CRendLock();
    CRendLock(unsigned long  dwSpinCount);
    ~CRendLock();
    void Lock();
    void Unlock();
    int  TryLock();
private:
		CRendLock(const CRendLock& cs);
		CRendLock operator = (const CRendLock& cs);
 
private:
        //�ٽ�ֵ
        void* m_crisec;
};
#endif