#ifndef renderD3D11_H_
#define renderD3D11_H_
#include "../KKPlayerCore/render/render.h"
class CRenderD3D11 : public IkkRender
{
public:
     CRenderD3D11(int         cpu_flags);
	 ~CRenderD3D11();
};
#endif