#ifndef   GnTextMgr_H_
#define   GnTextMgr_H_
class CGnTextMgr
{
	public:
		   ~CGnTextMgr(); 
		   static CGnTextMgr* GetIns();
		   void DrawText(char* txt,int len);
    private:
	       CGnTextMgr();
		   static CGnTextMgr*  pGnTextMgrIns;
};
#endif
