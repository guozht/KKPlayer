#include "AndPlayerStateNotifyMgr.h"
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
CAndPlayerStateNotifyMgr::CAndPlayerStateNotifyMgr():
     m_nThOver(1),
     m_nAddr(-1)
{
     
}
CAndPlayerStateNotifyMgr::~CAndPlayerStateNotifyMgr()
{

}

void CAndPlayerStateNotifyMgr::Start()
{
    LOGE("CAndPlayerStateNotifyMgr Start %d\n",m_nThOver);
	m_Lock.Lock();
    if(m_nThOver==1){
		
        m_nThOver=0;
		m_nAddr= pthread_create(&m_thTid_task, NULL, (void* (*)(void*))handleState_thread, (LPVOID)this);
    }
	m_Lock.Unlock();
    
}

void CAndPlayerStateNotifyMgr::Stop()
{
	m_nAddr=-1;
	while(m_nThOver==0)
	{
	     usleep(20000);
	}
	m_Lock.Lock();
	m_PlayerNotifyMap.clear();
	m_Lock.Unlock();
}
//将播放器状态添加到线程中
void CAndPlayerStateNotifyMgr::PutAState(int player,int state)
{
	m_Lock.Lock();
	m_playerstateQue.push(std::pair<int,int>(player,state));
	m_Lock.Unlock();
}

///设置播放器通知状态
void CAndPlayerStateNotifyMgr::SetPlayerNotify(JNIEnv *env,int obj,jobject notify)
{
    LOGE("SetPlayerNotify \n");
	m_Lock.Lock();
	std::map<int,jobject>::iterator It=m_PlayerNotifyMap.find(obj);
	if(It!=m_PlayerNotifyMap.end()){
        if(It->second!=notify){

            LOGE("Updata Notify \n");
            jobject RedObj = env->NewGlobalRef(notify);
            ///释放全局对象
            env->DeleteGlobalRef(It->second);
            It->second=RedObj;
        }
	}else{
        LOGE("new Notify \n");
        jobject RedObj = env->NewGlobalRef(notify);
		m_PlayerNotifyMap.insert(std::pair<int,jobject>(obj,RedObj));
		
	}
	m_Lock.Unlock();
}
void CAndPlayerStateNotifyMgr::RemovePlayerNotify(JNIEnv *env, int obj)
{
    m_Lock.Lock();
	std::map<int,jobject>::iterator It=m_PlayerNotifyMap.find(obj);
	if(It!=m_PlayerNotifyMap.end()){
        ///释放全局对象
        env->DeleteGlobalRef(It->second);
	    m_PlayerNotifyMap.erase(It);
	}
	m_Lock.Unlock();
}
///线程
unsigned CAndPlayerStateNotifyMgr::handleState_thread(LPVOID lpParameter)
{
	LOGE("CAndPlayerStateNotifyMgr thread \n");
	CAndPlayerStateNotifyMgr *mgr=(CAndPlayerStateNotifyMgr *)lpParameter;
	mgr->handleState();
	mgr->m_nThOver=1;
	return 0;
}

void* kk_jni_attach_env();
int kk_jni_detach_env();


extern jclass                    GIPlayerErrNotify;
extern jmethodID                 GOpenMediaStateNotifyId;
///状态通知
void  CAndPlayerStateNotifyMgr::handleState()
{
    JNIEnv *env =(JNIEnv*)kk_jni_attach_env();
	
    while(m_nAddr==0){
		int count=0;
	    m_Lock.Lock();
		count=m_playerstateQue.size();
		if(count>0){
			playerstate ss=m_playerstateQue.front();
			std::map<int,jobject>::iterator It=m_PlayerNotifyMap.find(ss.first);
			if(It!=m_PlayerNotifyMap.end()){
                LOGE("Call Notify \n");
				env->CallVoidMethod(It->second,GOpenMediaStateNotifyId,ss.first,ss.second);
			}else{
                LOGE("unfind NotifyMap \n");
            }
			m_playerstateQue.pop();
		}
		m_Lock.Unlock();
		if(count==0){
		    usleep(500000);
		}
	}

	kk_jni_detach_env();
    LOGE("handleState \n");
}