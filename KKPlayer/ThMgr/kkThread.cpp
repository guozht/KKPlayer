#include <windows.h>
 #include <process.h>
   #include <assert.h>
#include "kkThread.h"
#include <memory.h>
#include <stdio.h>
namespace kkBase
{
	CkkThread::CkkThread():m_nThStart(0)
	{
		memset(&m_ThInfo,0,sizeof(m_ThInfo));
		m_ThInfo.ThreadExit=true;
	}
	CkkThread::~CkkThread()
	{

	}
	 void CkkThread::run()
	 {
	     
	 }
	 void CkkThread::stop()
	 {
	      if(m_nThStart==0)
			  return ;
		  m_nThStart=2;
		  while(m_nThStart)
			  ::Sleep(10);
	 }
	void CkkThread::start()
	{
		if(m_nThStart==0)
		{
			m_nThStart=1;
	#ifdef _WINDOWS
			m_ThInfo.ThreadHandle=(HANDLE)_beginthreadex(NULL, NULL, RunThread, (LPVOID)this, 0,&m_ThInfo.ThreadAddr);
	#else
			m_ThInfo.ThreadAddr = pthread_create(&m_ThInfo.Tid_task, NULL, (void* (*)(void*))RunThread, (LPVOID)this);
	#endif
		}
	}
	bool CkkThread::isrun()
	{
	    return m_nThStart==1 ? true:false;
	}
	unsigned __stdcall CkkThread::RunThread(LPVOID lpParameter)
	{
		  printf("start thread \n");
	      CkkThread *pIns=(CkkThread *)lpParameter;
		  pIns->m_ThInfo.ThreadExit=false;
		  pIns->run(); 
		  pIns->m_ThInfo.ThreadExit=true;
		  pIns->m_nThStart=0;
		  return 0;
	}
}