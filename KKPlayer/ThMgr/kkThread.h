#ifndef kkThread_H_
#define kkThread_H_
namespace kkBase
{
	typedef struct KK_THREAD_INFO
{
#ifdef _WINDOWS
	void*         ThreadHandle;
#else
	pthread_t     Tid_task;
#endif
	unsigned      ThreadAddr;
	volatile bool ThreadExit;
}KK_THREAD_INFO;
	class CkkThread{
	    public:    
			    CkkThread();
				~CkkThread();
				 void start();
				 void stop();
	    protected:
			     bool isrun();
				 virtual void run();
				 static unsigned __stdcall RunThread(LPVOID lpParameter);
	    protected:
		         KK_THREAD_INFO          m_ThInfo;
				 volatile int            m_nThStart;

	};
}
#endif