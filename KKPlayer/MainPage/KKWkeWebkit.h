#ifndef KKWkeWebkit_H_
#define KKWkeWebkit_H_
#include "stdafx.h"
#define nullptr 0
#include <wkedefine.h>
#include <queue>
#include "Tool/CriSecLock.h"
#include "helper/SCriticalSection.h"
using namespace wke;
namespace SOUI
{   
    class KKWkeLoader
    {
    public:
        typedef void (*FunWkeInit)();
        typedef void (*FunWkeShutdown)();
        typedef wkeWebView (*FunWkeCreateWebView)();
        typedef void (*FunWkeDestroyWebView)(wkeWebView);
    public:
        KKWkeLoader();

        ~KKWkeLoader();

        BOOL Init(LPCTSTR pszDll);

        static KKWkeLoader* GetInstance();
    public:
        FunWkeCreateWebView m_funWkeCreateWebView;
        FunWkeDestroyWebView m_funWkeDestroyWebView;
    protected:
        HMODULE     m_hModWke;
        FunWkeInit  m_funWkeInit;
        FunWkeShutdown m_funWkeShutdown;
		int m_Exit;
        static KKWkeLoader * s_pInst;
    };

    class IWkeLoadNotify
	{
	    public:
			   virtual void WkeLoadNotify(int state)=0;
	};
    class KKWkeWebkit : public SWindow ,public ITimelineHandler,public wkeClientHandler
    {
        SOUI_CLASS_NAME( KKWkeWebkit, L"kkwkewebkit")
    public:
        KKWkeWebkit(void);
        ~KKWkeWebkit(void);
       
        IWebView* 	GetWebView(){return m_pWebView;}
		void focus();
		void  loadUrl(std::string url);
		void  SetNotify(IWkeLoadNotify* pWkeLoadNotify);
    protected:
        static void  OnTitleChanged(const struct _wkeClientHandler* clientHandler, const wkeString title);
        static void  OnUrlChanged(const struct _wkeClientHandler* clientHandler, const wkeString url);
		static void  OnWkeLoadingFinishCallback(wkeWebView webView, void* param, const wkeString url, wkeLoadingResult result, const wkeString failedReason);
		void         OnLoadingFinishCallback(wkeWebView webView, const wkeString url, wkeLoadingResult result, const wkeString failedReason);
		static void  OnWkePaintUpdatedCallback(wkeWebView webView, void* param, const HDC hdc, int x, int y, int cx, int cy);
		void         OnPaintUpdatedCallback(wkeWebView webView, const HDC hdc, int x, int y, int cx, int cy);
		void         OnNextFrame();
		
    protected:
        int          OnCreate(void *);
		void         IniWkeWeb();
		void         DestroyWkeWeb();
        void         OnDestroy();
        void         OnPaint(IRenderTarget *pRT);
        void OnSize(UINT nType, CSize size);
		BOOL OnEraseBkgnd(IRenderTarget *pRT);
        LRESULT OnMouseEvent(UINT uMsg, WPARAM wParam,LPARAM lParam); 
        LRESULT OnMouseWheel(UINT uMsg, WPARAM wParam,LPARAM lParam); 
        LRESULT OnKeyDown(UINT uMsg, WPARAM wParam,LPARAM lParam);
        LRESULT OnKeyUp(UINT uMsg, WPARAM wParam,LPARAM lParam);
        LRESULT OnChar(UINT uMsg, WPARAM wParam,LPARAM lParam);
        LRESULT OnImeStartComposition(UINT uMsg, WPARAM wParam,LPARAM lParam);
        void OnSetFocus(SWND wndOld);
        void OnKillFocus(SWND wndFocus);
        void OnTimer(UINT_PTR nIDEvent);

        virtual BOOL OnSetCursor(const CPoint &pt);
        virtual UINT OnGetDlgCode(){return SC_WANTALLKEYS;}
        BOOL OnAttrUrl(SStringW strValue, BOOL bLoading);
		BOOL OnAttrFile(SStringW strValue, BOOL bLoading);
        SOUI_ATTRS_BEGIN()
            ATTR_CUSTOM(L"url",OnAttrUrl)
			ATTR_CUSTOM(L"file",OnAttrFile)
        SOUI_ATTRS_END()

        SOUI_MSG_MAP_BEGIN()
            MSG_WM_PAINT_EX(OnPaint)
			MSG_WM_ERASEBKGND_EX(OnEraseBkgnd)
            MSG_WM_CREATE(OnCreate)
            MSG_WM_DESTROY(OnDestroy)
            MSG_WM_SIZE(OnSize)
            //MSG_WM_TIMER2(OnTimer)
            MSG_WM_SETFOCUS_EX(OnSetFocus)
            MSG_WM_KILLFOCUS_EX(OnKillFocus)
            MESSAGE_RANGE_HANDLER_EX(WM_MOUSEFIRST,0x209,OnMouseEvent)
            MESSAGE_HANDLER_EX(WM_MOUSEWHEEL,OnMouseWheel)
            MESSAGE_HANDLER_EX(WM_KEYDOWN,OnKeyDown)
            MESSAGE_HANDLER_EX(WM_KEYUP,OnKeyUp)
            MESSAGE_HANDLER_EX(WM_CHAR,OnChar)
            MESSAGE_HANDLER_EX(WM_IME_STARTCOMPOSITION,OnImeStartComposition)
        SOUI_MSG_MAP_END()

    protected:
        IWebView* m_pWebView;
        SStringW m_strUrl;
		SStringW m_strFile;
		IWkeLoadNotify* m_pWkeLoadNotify;
		int                           m_nOnSize;
		std::queue<wkeLoadingResult>  m_msgQue;
		 SCriticalSection             m_lock;
	
    };
}
#endif