#include "StdAfx.h"
#include "KKWkeWebkit.h"
#include <Imm.h>
#include <map>
#include "../Tool/cchinesecode.h"
#include "../Tool/WinDir.h"
#include "MainDlg.h"
#include <string>


extern SOUI::CMainDlg* G_pCMainDlg;
jsValue JS_CALL  FunMsgBox(jsExecState es)
{
	  int hh=jsToInt(es,jsArg(es,0));
     char *pp=(char*)jsToString(es,jsArg(es,1));
	 char *name=(char*)jsToString(es,jsArg(es,2));
	 std::string Out="";
	 std::string Out2="";
	 if(pp!=NULL){
		 CChineseCode::UTF_8ToGBK(Out,pp);
	 }
	  if(name!=NULL){
		 CChineseCode::UTF_8ToGBK(Out2,name);
	  }

	  if(hh!=1)
	  {
	  ::MessageBoxA((HWND)hh,Out.c_str(),Out2.c_str(),0);
	  }else{
	      ::MessageBoxA(0,Out.c_str(),Out2.c_str(),0);
	  }

	  return 0;
}

///播放器回调函数
jsValue JS_CALL  FunCallAVPlayer(jsExecState es)
{
     char *pp=(char*)jsToString(es,jsArg(es,0));
	 char *name=(char*)jsToString(es,jsArg(es,1));
	 if(pp!=NULL){
		 std::string Out="";
		 CChineseCode::UTF_8ToGBK(Out,pp);
		 if(G_pCMainDlg!=NULL){
			 G_pCMainDlg->WinTabShow(1);
			 G_pCMainDlg->OpenMedia((char*)Out.c_str(),name);
		 }/**/
		 
	 }
	 	return 0;
}
namespace SOUI
{

	
    //////////////////////////////////////////////////////////////////////////
    // KKWkeLoader
    KKWkeLoader * KKWkeLoader::s_pInst=0;
    
    KKWkeLoader* KKWkeLoader::GetInstance()
    {
        return s_pInst;
    }

    KKWkeLoader::KKWkeLoader() :m_hModWke(0),m_Exit(-1)
    {
        SASSERT(!s_pInst);
        s_pInst=this;
    }


    KKWkeLoader::~KKWkeLoader()
    {

        if(m_hModWke) 
			FreeLibrary(m_hModWke);
    }
    
    BOOL KKWkeLoader::Init( LPCTSTR pszDll )
    {
        if(m_hModWke) 
			return TRUE;

		
        wkeSettings* setting=0;
		wkeInitializeEx(setting);
        m_funWkeInit = (FunWkeInit)wkeInit;
        m_funWkeShutdown = (FunWkeShutdown)wkeShutdown;
        m_funWkeCreateWebView = (FunWkeCreateWebView)wkeCreateWebView;
        m_funWkeDestroyWebView = (FunWkeDestroyWebView)wkeDestroyWebView;
        if(!m_funWkeInit 
            || !m_funWkeShutdown
            || !m_funWkeCreateWebView
            || !m_funWkeDestroyWebView )
        {
            FreeLibrary(m_hModWke);
            return FALSE;
        }
		jsBindFunction("CallAVPlayer",FunCallAVPlayer,3);
		jsBindFunction("FunMsgBox",FunMsgBox,2);
        m_funWkeInit();
        return TRUE;
    }

    //////////////////////////////////////////////////////////////////////////
    // KKWkeWebkit
    
    KKWkeWebkit::KKWkeWebkit(void):m_pWebView(NULL),m_pWkeLoadNotify(0),m_nOnSize(0)
    {
		m_strUrl=L"";
		m_strFile=L"";
		
		 onTitleChanged=OnTitleChanged;
         onURLChanged=OnUrlChanged;

    }

	void  KKWkeWebkit::OnTitleChanged(const struct _wkeClientHandler* clientHandler, const wkeString title)
	{
	
	}
    void  KKWkeWebkit::OnUrlChanged(const struct _wkeClientHandler* clientHandler, const wkeString url)
	{
	
	}
    KKWkeWebkit::~KKWkeWebkit(void)
    {
		DestroyWkeWeb();
    }

	void KKWkeWebkit::focus()
	{
		if(m_pWebView){
			m_pWebView->setFocus();
	    }
	}
    void KKWkeWebkit::OnPaint(IRenderTarget *pRT)
    {


		CRect rcClip;
        pRT->GetClipBox(&rcClip);
        CRect rcClient;
        GetClientRect(&rcClient);
        CRect rcInvalid;
        rcInvalid.IntersectRect(&rcClip,&rcClient);
        HDC hdc=pRT->GetDC();
		if(m_nOnSize=1)
		{
			 HDC hdc2=wkeGetViewDC((wkeWebView)m_pWebView);
			if(GetStyle().m_byAlpha!=0xff)
			{
				BLENDFUNCTION bf={AC_SRC_OVER,0,GetStyle().m_byAlpha,AC_SRC_ALPHA };
				AlphaBlend(hdc,rcInvalid.left,rcInvalid.top,rcInvalid.Width(),rcInvalid.Height(),hdc2,rcInvalid.left-rcClient.left,rcInvalid.top-rcClient.top,rcInvalid.Width(),rcInvalid.Height(),bf);
			}else
			{
				BitBlt(hdc,rcInvalid.left,rcInvalid.top,rcInvalid.Width(),rcInvalid.Height(),hdc2,rcInvalid.left-rcClient.left,rcInvalid.top-rcClient.top,SRCCOPY);            
			}
		}
        pRT->ReleaseDC(hdc);
    }

    void KKWkeWebkit::OnSize( UINT nType, CSize size )
    {
        __super::OnSize(nType,size);	
		if(m_pWebView){
			m_nOnSize=1;
			    CRect rcClient;
		    GetClientRect(&rcClient);
			//wkeSetHandleOffset(( wkeWebView)m_pWebView,rcClient.left,rcClient.top);
            m_pWebView->resize(size.cx,size.cy);
		     wkeRepaintIfNeeded((wkeWebView)m_pWebView);
		}
    }
    BOOL KKWkeWebkit::OnEraseBkgnd(IRenderTarget *pRT)
	{
        m_style.m_bBkgndBlend=1;
	    return TRUE;
	}

	void  KKWkeWebkit::loadUrl(std::string url)
	{
		wchar_t wurl[2048]=L"";
		CChineseCode::charTowchar(url.c_str(),wurl,2048);

		int isHttp=0;
		if(strncmp(url.c_str(), "http:",5)==0||strncmp(url.c_str(), "https:",6)==0){
			isHttp=1;
		}
		if(isHttp){
			m_strUrl=wurl;
			m_strFile=L"";
			wkeLoadURLW((wkeWebView)m_pWebView,wurl);
			
		}else {

			CWinDir dir;
		    std::wstring weburl=dir.GetModulePath();
		    ::SetCurrentDirectory(weburl.c_str());
			m_strFile=wurl;
			m_strUrl=L"";
			wkeLoadFileW((wkeWebView)m_pWebView,wurl);
			
		}
		
    }
	void KKWkeWebkit::IniWkeWeb()
	{
		DestroyWkeWeb();
	    m_pWebView = (  IWebView*)KKWkeLoader::GetInstance()->m_funWkeCreateWebView();
		if(!m_pWebView) {
			assert(0);
			return ;
		}
	
		wkeOnLoadingFinish((wkeWebView)m_pWebView,OnWkeLoadingFinishCallback,this);
		wkeOnPaintUpdated((wkeWebView)m_pWebView, OnWkePaintUpdatedCallback,this);
		wkeSetCspCheckEnable((wkeWebView)m_pWebView,false);
		CRect rcClient;
		GetClientRect(&rcClient);

		
		m_pWebView->setClientHandler(this);
		m_pWebView->resize(rcClient.Width(),rcClient.Height());
        m_nOnSize=1;
		if(m_strUrl.GetLength()>5){
           m_pWebView->loadURL(m_strUrl);
		}
		else{
			if(m_strFile.GetLength()<1)
			return;
			m_pWebView->loadFile(m_strFile);
		}
	}
	void KKWkeWebkit::DestroyWkeWeb()
	{
		m_nOnSize=0;
	   if(m_pWebView)
		{
			KKWkeLoader::GetInstance()->m_funWkeDestroyWebView((wkeWebView)m_pWebView);
			m_pWebView=NULL;
		}
	}
	void  KKWkeWebkit::OnWkeLoadingFinishCallback(wkeWebView webView, void* param, const wkeString url, wkeLoadingResult result, const wkeString failedReason)
	{
	     KKWkeWebkit* ins=(KKWkeWebkit*)param;
		 ins->OnLoadingFinishCallback( webView,url,result, failedReason);
	}
	void  KKWkeWebkit::OnLoadingFinishCallback(wkeWebView webView,const wkeString url, wkeLoadingResult result, const wkeString failedReason)
	{
		m_lock. Enter();
		m_msgQue.push( result);
		m_lock.Leave();
		
	}
	void  KKWkeWebkit::OnWkePaintUpdatedCallback(wkeWebView webView, void* param, const HDC hdc, int x, int y, int cx, int cy)
	{
	     KKWkeWebkit* ins=(KKWkeWebkit*)param;
		 ins->OnPaintUpdatedCallback(webView,hdc, x,y,cx,cy);
	}
	void         KKWkeWebkit::OnPaintUpdatedCallback(wkeWebView webView, const HDC hdc, int x, int y, int cx, int cy)
	{
	    CRect rcClient;
		GetClientRect(&rcClient);

		rcClient.left+=x;
		rcClient.top+=y;

		rcClient.right=cx+rcClient.left;
		rcClient.bottom =cy+rcClient.top;
		InvalidateRect(rcClient);
	}
    int KKWkeWebkit::OnCreate( void * )
    {
		IniWkeWeb();
		GetContainer()->RegisterTimelineHandler(this);
        return 0;
	}

	void KKWkeWebkit::OnDestroy()
	{
		  GetContainer()->UnregisterTimelineHandler(this);
		if(m_pWebView)
		{
			KKWkeLoader::GetInstance()->m_funWkeDestroyWebView(( wkeWebView)m_pWebView);
			m_pWebView=NULL;
		}
	}

	LRESULT KKWkeWebkit::OnMouseEvent( UINT message, WPARAM wParam,LPARAM lParam)
	{
		if (message == WM_LBUTTONDOWN || message == WM_MBUTTONDOWN || message == WM_RBUTTONDOWN)
		{
			SetFocus();
			SetCapture();
		}
		else if (message == WM_LBUTTONUP || message == WM_MBUTTONUP || message == WM_RBUTTONUP)
		{
			ReleaseCapture();
		}

		CRect rcClient;
		GetClientRect(&rcClient);

		int x = GET_X_LPARAM(lParam)-rcClient.left;
		int y = GET_Y_LPARAM(lParam)-rcClient.top;

		unsigned int flags = 0;

		if (wParam & MK_CONTROL)
			flags |= WKE_CONTROL;
		if (wParam & MK_SHIFT)
			flags |= WKE_SHIFT;

		if (wParam & MK_LBUTTON)
			flags |= WKE_LBUTTON;
		if (wParam & MK_MBUTTON)
			flags |= WKE_MBUTTON;
		if (wParam & MK_RBUTTON)
			flags |= WKE_RBUTTON;

		bool bHandled = m_pWebView->fireMouseEvent(message, x, y, flags);
		SetMsgHandled(bHandled);
		return 0;
	}

	LRESULT KKWkeWebkit::OnKeyDown( UINT uMsg, WPARAM wParam,LPARAM lParam )
	{
		unsigned int flags = 0;
		if (HIWORD(lParam) & KF_REPEAT)
			flags |= WKE_REPEAT;
		if (HIWORD(lParam) & KF_EXTENDED)
			flags |= WKE_EXTENDED;

		SetMsgHandled(m_pWebView->fireKeyDownEvent(wParam, flags, false));
		return 0;
	}

	LRESULT KKWkeWebkit::OnKeyUp( UINT uMsg, WPARAM wParam,LPARAM lParam )
	{
		unsigned int flags = 0;
		if (HIWORD(lParam) & KF_REPEAT)
			flags |= WKE_REPEAT;
		if (HIWORD(lParam) & KF_EXTENDED)
			flags |= WKE_EXTENDED;

		SetMsgHandled(m_pWebView->fireKeyUpEvent(wParam, flags, false));
		return 0;
	}

	LRESULT KKWkeWebkit::OnMouseWheel( UINT uMsg, WPARAM wParam,LPARAM lParam )
	{
		POINT pt;
		pt.x = GET_X_LPARAM(lParam);
		pt.y = GET_Y_LPARAM(lParam);

		CRect rc;
		GetWindowRect(&rc);
		pt.x -= rc.left;
		pt.y -= rc.top;

		int delta = GET_WHEEL_DELTA_WPARAM(wParam);

		unsigned int flags = 0;

		if (wParam & MK_CONTROL)
			flags |= WKE_CONTROL;
		if (wParam & MK_SHIFT)
			flags |= WKE_SHIFT;

		if (wParam & MK_LBUTTON)
			flags |= WKE_LBUTTON;
		if (wParam & MK_MBUTTON)
			flags |= WKE_MBUTTON;
		if (wParam & MK_RBUTTON)
			flags |= WKE_RBUTTON;

		//flags = wParam;

		BOOL handled = m_pWebView->fireMouseWheelEvent(pt.x, pt.y, delta, flags);
		SetMsgHandled(handled);

		return handled;
	}

	LRESULT KKWkeWebkit::OnChar( UINT uMsg, WPARAM wParam,LPARAM lParam )
	{
		unsigned int charCode = wParam;
		unsigned int flags = 0;
		if (HIWORD(lParam) & KF_REPEAT)
			flags |= WKE_REPEAT;
		if (HIWORD(lParam) & KF_EXTENDED)
			flags |= WKE_EXTENDED;

		//flags = HIWORD(lParam);

		SetMsgHandled(m_pWebView->fireKeyPressEvent(charCode, flags, false));
		return 0;
	}

	LRESULT KKWkeWebkit::OnImeStartComposition( UINT uMsg, WPARAM wParam,LPARAM lParam )
	{
		wkeRect caret = m_pWebView->getCaret();

		CRect rcClient;
		GetClientRect(&rcClient);

		CANDIDATEFORM form;
		form.dwIndex = 0;
		form.dwStyle = CFS_EXCLUDE;
		form.ptCurrentPos.x = caret.x + rcClient.left;
		form.ptCurrentPos.y = caret.y + caret.h + rcClient.top;
		form.rcArea.top = caret.y + rcClient.top;
		form.rcArea.bottom = caret.y + caret.h + rcClient.top;
		form.rcArea.left = caret.x + rcClient.left;
		form.rcArea.right = caret.x + caret.w + rcClient.left;
		COMPOSITIONFORM compForm;
		compForm.ptCurrentPos=form.ptCurrentPos;
		compForm.rcArea=form.rcArea;
		compForm.dwStyle=CFS_POINT;

		HWND hWnd=GetContainer()->GetHostHwnd();
		HIMC hIMC = ImmGetContext(hWnd);
		ImmSetCandidateWindow(hIMC, &form);
		ImmSetCompositionWindow(hIMC,&compForm);
		ImmReleaseContext(hWnd, hIMC);
		return 0;
	}

	void KKWkeWebkit::OnSetFocus(SWND wndOld)
	{
	    __super::OnSetCursor(wndOld);
		m_pWebView->setFocus();
	}

	void KKWkeWebkit::OnKillFocus(SWND wndFocus)
	{
		m_pWebView->killFocus();
		__super::OnKillFocus(wndFocus);
	}

	
	
   void  KKWkeWebkit::OnNextFrame()
   {
        CRect rcClient;
		GetClientRect(&rcClient);
		/*if(m_pWebView!=NULL)
			m_pWebView->setDirty(true);*/

		m_lock.Enter();
		if(m_msgQue.size()>0){
		    wkeLoadingResult result=m_msgQue.front();

			if(result== WKE_LOADING_SUCCEEDED){
			   m_pWkeLoadNotify->WkeLoadNotify(1);
			}else if (result== WKE_LOADING_FAILED)
			{
			    m_pWkeLoadNotify->WkeLoadNotify(0);
				m_pWebView->loadHTML(L"无效网页！");
			}
			m_msgQue.pop();
		}
		m_lock.Leave();
   }
    

	BOOL KKWkeWebkit::OnSetCursor( const CPoint &pt )
	{
		return TRUE;
	}
    void  KKWkeWebkit::SetNotify(IWkeLoadNotify* pWkeLoadNotify)
	{
		m_pWkeLoadNotify=pWkeLoadNotify;
	}
	
	BOOL KKWkeWebkit::OnAttrUrl( SStringW strValue, BOOL bLoading )
	{
		m_strUrl=strValue;
		//m_strUrl=L"http://www.baidu.com/";//strValue;
		if(!bLoading)
			m_pWebView->loadURL(m_strUrl);
		return !bLoading;
	}
    BOOL  KKWkeWebkit::OnAttrFile(SStringW strValue, BOOL bLoading)
	{
		m_strFile=strValue;
		//m_strUrl=L"http://www.baidu.com/";//strValue;
		if(!bLoading)
			m_pWebView->loadFile(m_strFile);
		return !bLoading;
	}


}

