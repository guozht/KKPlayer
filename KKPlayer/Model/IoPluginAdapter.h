#ifndef IoPluginAdapter_H_
#define IoPluginAdapter_H_
#include "stdafx.h"
#include "UI.h"
#include <control/SCmnCtrl.h>
#include <helper/SAdapterBase.h>

#include <helper/SAdapterBase.h>
#include <control/SMCListViewEx.h>
#include <map>
#include <vector>
namespace SOUI
{
	struct IoDataItem{
	         wchar_t name[32];
			 wchar_t ver[32];
			 wchar_t author[64];
			 wchar_t brief[128];
			 IoDataItem()
			 {
			    memset(name,0,32);
				memset(ver,0,32);
				memset(author,0,64);
				memset(brief,0,128);
			 }
	};
	class CIoPluginAdapterFix : public SMcAdapterBaseEx
	{
		SArray<SStringW> m_colNames;
		public:
			    CIoPluginAdapterFix(SMCListViewEx *pSMCListView);
				~CIoPluginAdapterFix();

				
				void Refresh();
				int getCount();

				void SetData(std::vector<IoDataItem>& DataItemVector);
				SStringT getSizeText(DWORD dwSize);
				bool OnItemClick(EventArgs *pEvt);
				virtual void getView(int position, SWindow * pItem,pugi::xml_node xmlTemplate);

				bool OnButtonClick(EventArgs *pEvt);
				SStringW GetColumnName(int iCol) const;
				
				bool OnSort(int iCol,SHDSORTFLAG * stFlags,int nCols);

				bool OnItemDelete(EventArgs *pEvt);
				bool OnItemPause(EventArgs *pEvt);
				bool OnItemOpenLocal(EventArgs *pEvt);

				bool SwapItem(long pos,SList<long> &){return false;}

				void IniColNames(pugi::xml_node xmlTemplate)
				{
					for (xmlTemplate = xmlTemplate.first_child(); xmlTemplate; xmlTemplate = xmlTemplate.next_sibling())
					{
						m_colNames.Add(xmlTemplate.attribute(L"name").value());
					}
				}

	
				virtual void InitByTemplate(pugi::xml_node xmlTemplate)
				{
					IniColNames(xmlTemplate);
				}
		private:
		
			SMCListViewEx *m_pSMCListView;
			std::vector<IoDataItem> m_DataItemVector;
			
	};
}
#endif