#include "IoPluginAdapter.h"

namespace SOUI
{
    CIoPluginAdapterFix::CIoPluginAdapterFix(SMCListViewEx *pSMCListView)
	{
	    m_pSMCListView=pSMCListView;
	}
	CIoPluginAdapterFix::~ CIoPluginAdapterFix()
	{
	
	}
	void  CIoPluginAdapterFix::Refresh()
	{
	  
	}
	int CIoPluginAdapterFix::getCount()
	{
	     return m_DataItemVector.size();
	}
	void CIoPluginAdapterFix::SetData(std::vector<IoDataItem>& DataItemVector)
	{
	  m_DataItemVector=DataItemVector;
	}
    SStringT CIoPluginAdapterFix::getSizeText(DWORD dwSize)
	{
	        int num1=dwSize/(1<<20);
			dwSize -= num1 *(1<<20);
			int num2 = dwSize*100/(1<<20);
			return SStringT().Format(_T("%d.%02dM"),num1,num2);
	}
	bool CIoPluginAdapterFix::OnItemClick(EventArgs *pEvt)
	{
	     
			return true;
	}
	bool CIoPluginAdapterFix::OnItemDelete(EventArgs *pEvt)
	{
		
	    return true;
	}
    bool  CIoPluginAdapterFix::OnItemPause(EventArgs *pEvt)
	{
		
	    return true;
	}
	bool  CIoPluginAdapterFix::OnItemOpenLocal(EventArgs *pEvt)
    {
   
	   return true;
	}

	
	void CIoPluginAdapterFix::getView(int position, SWindow * pItem,pugi::xml_node xmlTemplate)
	{

		int        len=2*1024*4;
		char*      temp2=(char*)::malloc(4096);
		wchar_t*   temp = (wchar_t  *)::malloc(len);

		if(pItem->GetChildrenCount()==0){
			pItem->InitFromXml(xmlTemplate);
		}
      
		IoDataItem& DataItem=m_DataItemVector.at(position);
		pItem->FindChildByName("txt_Name")->SetWindowText(DataItem.name);
		pItem->FindChildByName("txt_Ver")->SetWindowText(DataItem.ver);

		pItem->FindChildByName("txt_Author")->SetWindowText(DataItem.author);
	    return ;
	}
    bool CIoPluginAdapterFix::OnButtonClick(EventArgs *pEvt)
	{
	   return 1;
	}
	SStringW CIoPluginAdapterFix::GetColumnName(int iCol) const
	{
	  return SStringW().Format(L"col%d",iCol+1);
	}
	bool CIoPluginAdapterFix::OnSort(int iCol,SHDSORTFLAG * stFlags,int nCols)
	{
	   return true;
	}
}