#include "uiinfo.h"
#include "SqlOp/HistoryInfoMgr.h"
#define PlayerVol            "PlayerVol"
#define PlayerMainTab        "PlayerMainTab"
#define PlayerResUrl         "PlayerResUrl"
#define PlayerStoreDir       "PlayerStoreDir"
int   CUiInfo::GetStoreVolume()
{
      CHistoryInfoMgr *InfoMgr=CHistoryInfoMgr::GetInance();
	  std::string strvol;
	  bool Ok=InfoMgr->GetConfig(PlayerVol,strvol);
	  if(Ok)
	  return atoi(strvol.c_str());

	  return 100;
}
void CUiInfo::SetStoreVolume(int vol)
{
	  char strvol[64]="";
	  sprintf(strvol,"%d",vol);
	  CHistoryInfoMgr *InfoMgr=CHistoryInfoMgr::GetInance();
	  InfoMgr->UpdataConfig(PlayerVol,strvol);
}
int   CUiInfo::GetMainTab()
{
      CHistoryInfoMgr *InfoMgr=CHistoryInfoMgr::GetInance();
	  std::string strvol;
	  bool Ok=InfoMgr->GetConfig(PlayerMainTab,strvol);
	  if(Ok)
	  return atoi(strvol.c_str());

	  return 2;
}
void  CUiInfo::SetMainTab(int tab)
{
      char strvol[64]="";
	  sprintf(strvol,"%d",tab);
	  CHistoryInfoMgr *InfoMgr=CHistoryInfoMgr::GetInance();
	  InfoMgr->UpdataConfig(PlayerMainTab,strvol);
}
void                     CUiInfo::SetResUrl(const char* url)
{
    
	  CHistoryInfoMgr *InfoMgr=CHistoryInfoMgr::GetInance();
	  InfoMgr->UpdataConfig(PlayerResUrl,url);
}

std::string& trim(std::string &s)   
{  
    if (s.empty())   
    {  
        return s;  
    }  
  
    s.erase(0,s.find_first_not_of(" "));  
    s.erase(s.find_last_not_of(" ") + 1);  
    return s;  
}  

std::string              CUiInfo::GetResUrl()
{
     CHistoryInfoMgr *InfoMgr=CHistoryInfoMgr::GetInance();
	  std::string strvol;
	  bool Ok=InfoMgr->GetConfig(PlayerResUrl,strvol);
	  
	  strvol=trim(strvol);
	  if(strvol.length()<2)
	     return "ui/Res.html";
	  return strvol;
	 //  return "https://otcbtc.com/";
}

std::string              CUiInfo::GetStoreDir()
{
      CHistoryInfoMgr *InfoMgr=CHistoryInfoMgr::GetInance();
	  std::string strvol;
	  bool Ok=InfoMgr->GetConfig(PlayerStoreDir,strvol);
	  return strvol;
}
void                     CUiInfo::SetStoreDir(const char* dir)
{
      CHistoryInfoMgr *InfoMgr=CHistoryInfoMgr::GetInance();
	  InfoMgr->UpdataConfig(PlayerStoreDir,dir);
}