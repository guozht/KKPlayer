package com.ic70.kkplayer.kkplayer;

/**
 * Created by saint on 2018/3/21.
 */

public class CGlobalInfo {
    static int Screenwidth=0;
    static int Screenheight=0;
    static int AvItemWidth=0;
    static int AvItemHeight=0;

    public  static  void SetScreenWidth(int w){
        Screenwidth=w;
    }

    public  static  void SetScreenheight(int h){
        Screenheight=h;
    }

    public  static  int GetScreenWidth(){
        return Screenwidth;
    }

    public  static  int GetScreenheight(){
        return Screenheight;
    }

    public  static  int GetAvItemWidth(){
        if(AvItemWidth==0)
            AvItemWidth=CGlobalInfo.GetScreenWidth()/3;


        return AvItemWidth;
    }

    public  static  int GetAvItemHeight(){
        if(AvItemHeight==0) {
            AvItemWidth = CGlobalInfo.GetScreenWidth() / 3;
            AvItemHeight =AvItemWidth * 5 / 3;
        }
        return AvItemHeight;
    }


}
