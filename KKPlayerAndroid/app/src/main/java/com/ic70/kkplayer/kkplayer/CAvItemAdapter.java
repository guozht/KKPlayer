package com.ic70.kkplayer.kkplayer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.bumptech.glide.Glide;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * Created by saint on 2018/3/21.
 */

public class CAvItemAdapter extends BaseAdapter {

    private Context context;
    private List<CAVInfoModel> array;
    CAvItemAdapter(Context context,List<CAVInfoModel> array){
        this.context=context;
        this.array=array;
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return array.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return array.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        int ImgWidth=CGlobalInfo.GetScreenWidth()/3;
        int ImgHeight=ImgWidth* 5/3;
        int txtHeight=ImgHeight/5;
        int height=ImgHeight+txtHeight;
        if(convertView==null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.av_grid_item, null);
            android.widget.AbsListView.LayoutParams params=new  android.widget.AbsListView.LayoutParams(ImgWidth,height);
            //ViewGroup.LayoutParams  params=new  LinearLayout.LayoutParams(width,height);
            convertView.setLayoutParams(params);
        }

        ////
        {
            LinearLayout imgRelay = (LinearLayout) convertView.findViewById(R.id.imgRelay);
            LinearLayout.LayoutParams imgParams = (LinearLayout.LayoutParams) imgRelay.getLayoutParams();
            imgParams.height = ImgHeight;
            imgParams.width = ImgWidth;
            imgRelay.setLayoutParams(imgParams);
        }

        CAVInfoModel model= array.get(position);
        ImageView img= (ImageView)convertView.findViewById(R.id.imgView);
        img.setOnClickListener( new ItemClick(model));
        Glide.with(context)
                .load(model.m_strpicurl)
                .override(ImgWidth-10,ImgHeight-10)
                .placeholder(R.drawable.wait32)
                .into(img);

        TextView txtView=(TextView)convertView.findViewById(R.id.txtAvTitle);
        txtView.setText(model.m_stravname);
        return convertView;
    }
    class ItemClick  implements View.OnClickListener
    {
         private CAVInfoModel m_model=null;
         public ItemClick(CAVInfoModel model)
         {
             m_model=model;
         }
          public   void onClick(View v)
          {
              Intent intent = new Intent();
              intent.putExtra("MoviePath",  m_model.m_stravurl);

              int kkMediacodec=0;
              int     RaType=1;  //拉伸
              // RaType=1;      原始比例
              // RaType=2;      4:3
              // RaType=3;      16:9
              intent.putExtra("RaType",Integer.toString(RaType));
               intent.putExtra("kkMediacodec",Integer.toString(kkMediacodec));
              //指定intent要启动的类

              //kkMediacodec
              //intent.setClass(v.getContext(), CPlayerActivity.class);//(context.this, Activity02.class);
              intent.setClass(v.getContext(), avStreamActivity.class);

              //启动一个新的Activity
              v.getContext().startActivity(intent);
          }
    }
    protected void onCheckBoxClick(View view,String UserData)
    {

    }
}
