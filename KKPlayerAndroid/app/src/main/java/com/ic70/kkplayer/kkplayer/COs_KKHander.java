package com.ic70.kkplayer.kkplayer;

import android.os.Message;

import java.util.List;

/**
 * Created by saint on 2016/4/11.
 */
public class COs_KKHander extends android.os.Handler
{
    public  static  final  int                 MSG_LoadFile=100;
    public  static  final  int                 MSG_CancelWaitPop=101;
    private IKKMessageHandler m_KKMessageHandler;

    public COs_KKHander(IKKMessageHandler obj)
    {

        m_KKMessageHandler=obj;
    }
    public void handleMessage(Message msg)
    {
        m_KKMessageHandler.HandleKKObj(msg);
        //super.handleMessage(msg);
    }
    public class Os_Message
    {
        public Os_Message(){

        }
        public Object obj;
        public Object obj2;
    };
}
