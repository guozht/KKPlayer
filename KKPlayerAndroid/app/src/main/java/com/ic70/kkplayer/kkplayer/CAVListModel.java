package com.ic70.kkplayer.kkplayer;

import java.util.ArrayList;
import java.util.List;
import com.ic70.kkplayer.kkplayer.CAVInfoModel;
/**
 * Created by saint on 2018/3/21.
 */

public class CAVListModel
{
    private List<CAVInfoModel>    m_avlist;
    private CAvItemAdapter        m_ItemAdapter = null;
    public  String                m_txtTitleRegion;
    public CAVListModel()
    {
        m_avlist = new ArrayList<CAVInfoModel>();
    }
    public boolean AddAv(CAVInfoModel av) {
        return m_avlist.add(av);
    }
    public boolean DelAv(CAVInfoModel av) {
        return m_avlist.remove(av);
    }
    public void SetItemAdapter(CAvItemAdapter ItemAdapter){
        m_ItemAdapter = ItemAdapter;
    }
    public  CAvItemAdapter GetItemAdapter() {
       return m_ItemAdapter;
    }
    public List<CAVInfoModel> GetList(){ return m_avlist;}
}
