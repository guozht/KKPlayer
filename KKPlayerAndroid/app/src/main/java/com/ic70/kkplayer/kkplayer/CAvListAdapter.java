package com.ic70.kkplayer.kkplayer;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import com.ic70.kkplayer.kkplayer.CAVListModel;
/**
 * Created by saint on 2018/3/21.
 */

public class CAvListAdapter extends RecyclerView.Adapter<CAvListAdapter.AvViewHolder> {
    private Context context;
    private List<CAVListModel> array;

    class AvViewHolder extends RecyclerView.ViewHolder {
        public AvViewHolder(View view)
        {
            super(view);
        }
    }

    @Override
    public AvViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = inflater.inflate(R.layout.av_recyer_item ,null);
        AvViewHolder holder = new AvViewHolder( convertView );
        return holder;
    }

    public void onBindViewHolder(AvViewHolder holder, int position)
    {
        GridView view=(GridView) holder.itemView.findViewById(R.id.listavitemgrid);
        CAVListModel model= array.get(position);
        CAvItemAdapter ItemAdapter=model.GetItemAdapter();
        if(ItemAdapter==null){
            ItemAdapter= new CAvItemAdapter(context,model.GetList());
            model.SetItemAdapter(ItemAdapter);
        }
        view.setAdapter(ItemAdapter);

        TextView txt= (TextView)holder.itemView.findViewById(R.id.txtTitleRegion);
        txt.setText(model.m_txtTitleRegion);
        int count=ItemAdapter.getCount()/3;
        if(ItemAdapter.getCount()%3!=0){
            count++;
        }
        int width  = CGlobalInfo.GetScreenWidth();
        int txtHeight=CGlobalInfo.GetAvItemHeight()/5+CGlobalInfo.GetAvItemHeight();
        int height = count* txtHeight;


        ViewGroup.LayoutParams  params=new  LinearLayout.LayoutParams(width,height);
        view.setLayoutParams(params);
        //holder.itemView.setLayoutParams(params);
        // Log.v("tag:","Id"+position);
    }

    CAvListAdapter(Context context,List<CAVListModel> array){
        this.context=context;
        this.array=array;
    }
    @Override
    public int getItemCount()
    {
        return array.size();
    }
}
